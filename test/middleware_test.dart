import 'package:employeeviewer/api/auth/api_configuration.dart';
import 'package:employeeviewer/api/employee_api.dart';
import 'package:employeeviewer/api/profile_api.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'middleware_test.mocks.dart';

@GenerateMocks([http.Client])
void main(store) {
  group('fetchEmployeeList', () {
    test('returns an EmployeeList if the http call completes successfully',
        () async {
      final client = MockClient();
      when(client.get(
              Uri.parse(AuthApiConfiguration.instance.employeeListEndpoint)))
          .thenAnswer((_) async => http.Response(
              '[{"id": "ev","fullName": "Employee Viewer",'
              '"email": "ev@leobit.com","active": true,'
              '"employment": "FT","paymentMode": "Leobit Card",'
              '"location": "Office","peManagement": null,'
              '"paidVacationFrom": null,"startDate": "2022-01-12T00:00:00",'
              '"terminationDate": null,"intHours": 304.00,'
              '"seniority": "Trainee","senioritySublevel": "Trainee",'
              '"profile": "Flutter","position": "Software Engineer",'
              '"phone": "380841957104",'
              '"photoSmall": "",'
              '"photoMedium": "",'
              '"skype": null,'
              '"photo": "",'
              '"dismissal": null,"birthday": null,"manager": "Time Tracking",'
              '"mentor": "Time Tracking","yearHired": 2022,"yearDismissed": null,'
              '"roomNumber": 502,"englishLevel": "Intermediate Strong",'
              '"careerStart": null,"certificates": null,"keInfo": null,'
              '"expectedPromotion": null,"projects": ["Leobit: Internal - Test"]},'
              '{"id": "tt","fullName": "Time Tracking",'
              '"email": "tt@leobit.com","active": true,'
              '"employment": "FT","paymentMode": "Leobit Card",'
              '"location": "Office","peManagement": null,'
              '"paidVacationFrom": null,"startDate": "2022-01-12T00:00:00",'
              '"terminationDate": null,"intHours": 304.00,'
              '"seniority": "Senior","senioritySublevel": "Senior",'
              '"profile": "Flutter","position": "Software Engineer",'
              '"phone": "380958274926",'
              '"photoSmall": "",'
              '"photoMedium": "",'
              '"skype": null,'
              '"photo": "",'
              '"dismissal": null,"birthday": null,"manager": "",'
              '"mentor": "","yearHired": 2022,"yearDismissed": null,'
              '"roomNumber": 502,"englishLevel": "Intermediate Strong",'
              '"careerStart": null,"certificates": null,"keInfo": null,'
              '"expectedPromotion": null,"projects": ["Leobit: Internal - TestingTest"]}'
              ']',
              200));
      store.dispatch(DownloadEmployeeList(
          (await EmployeeApi(client).getEmployeeDataTest())!));

      expect(store.state.employeeList.length, 2);
    });
    test(
        'returns an EmployeeDetails (ev) if the http call completes successfully',
        () async {
      final client = MockClient();
      when(client.get(Uri.parse(
              '${AuthApiConfiguration.instance.employeeListEndpoint}/ev')))
          .thenAnswer((_) async => http.Response(
              '{"id": "ev","fullName": "Employee Viewer",'
              '"email": "ev@leobit.com","active": true,'
              '"employment": "FT","paymentMode": "Leobit Card",'
              '"location": "Office","peManagement": null,'
              '"paidVacationFrom": null,"startDate": "2022-01-12T00:00:00",'
              '"terminationDate": null,"intHours": 304.00,'
              '"seniority": "Trainee","senioritySublevel": "Trainee",'
              '"profile": "Flutter","position": "Software Engineer",'
              '"phone": "380841957104",'
              '"photoSmall": "",'
              '"photoMedium": "",'
              '"skype": null,'
              '"photo": "",'
              '"dismissal": null,"birthday": null,"manager": "Time Tracking",'
              '"mentor": "Time Tracking","yearHired": 2022,"yearDismissed": null,'
              '"roomNumber": 502,"englishLevel": "Intermediate Strong",'
              '"careerStart": null,"certificates": null,"keInfo": null,'
              '"expectedPromotion": null,"projects": ["Leobit: Internal - Test"]}',
              200));
      store.dispatch(DownloadEmployeeDetails(
          (await EmployeeApi(client).getEmployeeDetailsTest('ev'))!));

      expect(store.state.employeeData.id, 'ev');
    });

    test(
        'returns an EmployeeProfile data (ev) if the http call completes successfully',
        () async {
      final client = MockClient();
      when(client.get(
              Uri.parse(AuthApiConfiguration.instance.personalInfoEndpoint)))
          .thenAnswer((_) async => http.Response(
              '{"sub": "ev","name": "Employee Viewer",'
              '"email": "ev@leobit.com","picture": ""}',
              200));
      store.state.profileData = (await ProfileApi(client).getDataTest());

      expect(store.state.profileData.sub, 'ev');
    });
  });
}
