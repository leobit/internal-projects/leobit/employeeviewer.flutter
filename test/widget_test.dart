import 'package:employeeviewer/components/employee_details/employee_view.dart';
import 'package:employeeviewer/components/employee_list/employee_list_view.dart';
import 'package:employeeviewer/components/filter_view/filter_view.dart';
import 'package:employeeviewer/components/settings_view/setings_view.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/reducer/tab_navigation_reducer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:redux/redux.dart';

void main(store) {
  testWidgets('test render employee list', (WidgetTester tester) async {
    await mockNetworkImagesFor(() => tester.pumpWidget(StoreProvider(
          store: Store<TabNavigationState>(tabNavigationReducer,
              initialState: TabNavigationState.init),
          child: StoreProvider(
              store: store as Store<AppState>,
              child: MaterialApp(
                  home: StoreConnector<AppState, AppState>(
                      converter: (store) => store.state,
                      builder: (context, vm) => const EmployeeListView()))),
        )));
    await tester.pump();

    expect(find.text(store.state.employeeList.first.email), findsOneWidget);
    expect(find.text(store.state.employeeList.last.email), findsOneWidget);
  });

  testWidgets('test filter section and dropdown generation',
      (WidgetTester tester) async {
    await tester.pumpWidget(StoreProvider(
      store: Store<TabNavigationState>(tabNavigationReducer,
          initialState: TabNavigationState.init),
      child: StoreProvider(
          store: store as Store<AppState>,
          child: MaterialApp(
              home: StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (context, vm) => const FilterView()))),
    ));
    expect(find.text('Manager name'), findsOneWidget);
    await tester.tap(find.text('Manager name'));
    await tester.pumpAndSettle();
    expect(find.text(store.state.employeeList.first.manager as String),
        findsOneWidget);
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();
    expect(find.text(store.state.employeeList.first.manager as String),
        findsNothing);
    expect(find.byIcon(Icons.menu), findsNothing);
  });

  testWidgets('test render employee details view', (WidgetTester tester) async {
    await mockNetworkImagesFor(() => tester.pumpWidget(StoreProvider(
          store: Store<TabNavigationState>(tabNavigationReducer,
              initialState: TabNavigationState.init),
          child: StoreProvider(
              store: store as Store<AppState>,
              child: StoreProvider(
                store: Store<TabNavigationState>(tabNavigationReducer,
                    initialState: TabNavigationState.init),
                child: MaterialApp(
                    home: StoreConnector<AppState, AppState>(
                        converter: (store) => store.state,
                        builder: (context, vm) => const EmployeeView())),
              )),
        )));
    await tester.pump();

    expect(find.text(store.state.employeeData.email), findsOneWidget);
    expect(find.text(store.state.employeeData.roomNumber), findsNothing);
  });

  testWidgets('testing settings view', (WidgetTester tester) async {
    await tester.pumpWidget(StoreProvider(
      store: Store<TabNavigationState>(tabNavigationReducer,
          initialState: TabNavigationState.init),
      child: StoreProvider(
          store: store as Store<AppState>,
          child: MaterialApp(
              home: StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (context, vm) => const SettingsView()))),
    ));
    expect(find.text('Night mode'), findsOneWidget);
    expect(find.byKey(const Key('settings_appBar')), findsOneWidget);
    expect(find.byKey(const Key('employee_list')), findsNothing);
  });
}
