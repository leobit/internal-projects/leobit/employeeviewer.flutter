import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/reducer/reducer.dart';
import 'package:redux/redux.dart';

import 'middleware_test.dart' as middleware_test;
import 'reducer_test.dart' as reducer_test;
import 'widget_test.dart' as widget_test;

void main() {
  final Store<AppState> store =
      Store<AppState>(reducer, initialState: AppState.init);

  middleware_test.main(store);
  reducer_test.main(store);
  widget_test.main(store);
}
