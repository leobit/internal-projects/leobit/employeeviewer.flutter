import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter_test/flutter_test.dart';

void main(store) {
  group('State Reducer', () {
    const String key = 'Name';
    const String keyValue = 'Employee';
    final List clearValue = [
      'All',
      'All',
      '',
      '',
      '',
      '',
      '',
      [],
      [],
      [],
      [],
      [],
    ];
    test('should change theme state', () async {
      store.dispatch(SetThemeAction(!store.state.themeState));

      expect(store.state.themeState, !AppState.init.themeState);
    });

    test('should update filter', () async {
      store.dispatch(UpdateFilterValueAction(key, keyValue));

      expect(store.state.filterValue[key], keyValue);
    });

    test('should update filter', () async {
      store.dispatch(UpdateFilterValueAction(key, keyValue));

      expect(store.state.filterValue[key], keyValue);
    });
    test('should clear filter', () async {
      store.dispatch(ClearFilerAction());

      expect(store.state.filterValue.values, clearValue);
    });
  });
}
