import 'dart:async';
import 'dart:io';

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/notification.dart';
import 'package:employeeviewer/components/corporate_rules/corporate_rules_view.dart';
import 'package:employeeviewer/components/employee_details/employee_certificates_view.dart';
import 'package:employeeviewer/components/employee_details/employee_view.dart';
import 'package:employeeviewer/components/employee_list/employee_list_view.dart';
import 'package:employeeviewer/components/favorite_employee_list/favorite_employee_view.dart';
import 'package:employeeviewer/components/filter_view/filter_view.dart';
import 'package:employeeviewer/components/login/login.dart';
import 'package:employeeviewer/components/navigation/ios/more_section.dart';
import 'package:employeeviewer/components/notification_list/archive_notification.dart';
import 'package:employeeviewer/components/notification_list/notification_list.dart';
import 'package:employeeviewer/components/statistic/employee_certificate_list.dart';
import 'package:employeeviewer/components/statistic/statistic_view.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/reducer/reducer.dart';
import 'package:employeeviewer/redux/reducer/tab_navigation_reducer.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:employeeviewer/template/more_items.dart';
import 'package:employeeviewer/template/typography.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:timezone/data/latest_all.dart' as tz;

import 'api/auth/api_configuration.dart';
import 'components/settings_view/setings_view.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) {
        return AuthApiConfiguration.instance.allowedHosts.contains(host);
      };
  }
}

Future<void> initLocalNotification() async {
  var initializationSettingsAndroid = const AndroidInitializationSettings(
    'ic_launcher',
  );
  var initializationSettingsIOS = DarwinInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
    onDidReceiveLocalNotification: (id, title, body, payload) async {},
  );
  var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  await FlutterLocalNotificationsPlugin().initialize(initializationSettings,
      onDidReceiveNotificationResponse: (payload) async {
    debugPrint('notification payload: $payload');
  });
}

Future<void> initHiveDB() async {
  await Hive.initFlutter();
  Hive.registerAdapter(EmployeeDataAdapter());
  Hive.registerAdapter(PersonalDataAdapter());
  Hive.registerAdapter(NotificationUpdateAdapter());
}

void main() async {
  runZonedGuarded<Future<void>>(() async {
    await initHiveDB();
    tz.initializeTimeZones();
    await initLocalNotification();
    HttpOverrides.global = MyHttpOverrides();
    WidgetsFlutterBinding.ensureInitialized();
    final store = Store<AppState>(reducer,
        initialState: AppState.init, middleware: [thunkMiddleware]);
    await Firebase.initializeApp();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    runApp(MyApp(store: store));
  }, (error, stack) => FirebaseCrashlytics.instance.recordError(error, stack));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.store});

  final Store<AppState> store;

  @override
  Widget build(BuildContext context) {
    return StoreProvider(store: store, child: MainBody(store: store));
  }
}

class MainBody extends StatelessWidget {
  const MainBody({super.key, required this.store});

  final Store<AppState> store;

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: Store<TabNavigationState>(tabNavigationReducer,
          initialState: TabNavigationState.init),
      child: StoreConnector<AppState, AppState>(
          converter: (store) => store.state,
          builder: (context, state) {
            return MaterialApp(
              theme: ThemeData(
                primaryColor: Colors.white,
                canvasColor: Colors.white,
                cardColor: Colors.grey.shade400.withOpacity(0.4),
                primaryColorLight: Colors.black.withOpacity(0.4),
                scaffoldBackgroundColor: Platform.isIOS
                    ? CupertinoColors.extraLightBackgroundGray
                    : AppColors.white,
                brightness: Brightness.light,
                textTheme: TextTheme(
                  displayLarge: AppTypography.headline1,
                  displayMedium: AppTypography.headline2,
                  displaySmall: AppTypography.headline3,
                  headlineMedium: AppTypography.headline4,
                  headlineSmall: AppTypography.headline5,
                  titleLarge: AppTypography.headline6,
                ),
                colorScheme: ColorScheme.light(
                  primary: AppColors.leoColor,
                  background: AppColors.white,
                ),
              ),
              darkTheme: ThemeData(
                scaffoldBackgroundColor: CupertinoColors.darkBackgroundGray,
                canvasColor: Colors.green.shade100.withOpacity(0.1),
                cardColor: Colors.green.shade100.withOpacity(0.1),
                primaryColorLight: Colors.white.withOpacity(0.7),
                brightness: Brightness.dark,
                textTheme: TextTheme(
                  displayLarge: AppTypography.darkThemeHeadline1,
                  displayMedium: AppTypography.darkThemeHeadline2,
                  displaySmall: AppTypography.darkThemeHeadline3,
                  headlineMedium: AppTypography.darkThemeHeadline4,
                  headlineSmall: AppTypography.darkThemeHeadline5,
                  titleLarge: AppTypography.darkThemeHeadline6,
                ),
                colorScheme: ColorScheme.dark(
                  primary: AppColors.leoColor,
                  background: CupertinoColors.placeholderText,
                ),
              ),
              themeMode: (state.themeState) ? ThemeMode.dark : ThemeMode.light,
              debugShowCheckedModeBanner: false,
              title: 'Leobit Demo',
              routes: {
                '/': (context) => const LoginScreen(),
                '/list': (context) => const EmployeeListView(),
                '/filter': (context) => const FilterView(),
                '/details': (context) => const EmployeeView(),
                '/settings': (context) => const SettingsView(),
                '/favorite': (context) => const FavoriteEmployeeView(),
                '/notification': (context) => const NotificationList(),
                '/archived-notification': (context) =>
                    const ArchiveNotification(),
                '/corporate-rules': (context) => const CorporateRulesView(),
                '/statistic': (context) => const StatisticView(),
                '/more': (context) => MoreSection(sectionItems: moreItemList),
                '/certificate-statistic': (context) =>
                    const EmployeeCertificateList(),
                '/certificates': (context) => const EmployeeCertificatesView(),
              },
            );
          }),
    );
  }
}
