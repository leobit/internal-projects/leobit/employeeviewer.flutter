import 'dart:convert';

import 'package:employeeviewer/api/auth/api_configuration.dart';
import 'package:employeeviewer/api/auth/auth_api.dart';
import 'package:employeeviewer/api/model/api_models.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:http/http.dart' as http;

class EmployeeApi {
  EmployeeApi(this.client);

  final AuthApi auth = AuthApi();
  final http.Client client;

  Future<List<EmployeeData>?> getEmployeeData() async {
    final headers = await auth.buildHeaders();

    var response = await client.get(
        Uri.parse(AuthApiConfiguration.instance.employeeListEndpoint),
        headers: headers);

    try {
      FirebaseCrashlytics.instance
          .log('Employee List Data|INFO|${DateTime.now().toString()}:\n'
              '{\nResponse code: ${response.statusCode}\n}');

      if (response.statusCode != 200) {
        throw Exception('Failed to fetch employee list');
      }

      final decoded = jsonDecode(response.body);

      return decoded is List
          ? decoded.map((json) => EmployeeData.fromJson(json)).toList()
          : [];
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance.recordError(error, stackTrace,
          reason:
              'Failed fetch employee list (Response code:${response.statusCode})');
      return null;
    }
  }

  Future<EmployeeData?> getEmployeeDetails(String employeeId) async {
    final headers = await auth.buildHeaders();
    var response = await client.get(
        Uri.parse(
            '${AuthApiConfiguration.instance.employeeListEndpoint}/$employeeId'),
        headers: headers);

    try {
      FirebaseCrashlytics.instance.log(
          'Employee Details|INFO|${DateTime.now().toString()}:\n'
          '{\nEmployee ID: $employeeId\nResponse code: ${response.statusCode}\n}');
      final dataCollection = jsonDecode(response.body);
      return EmployeeData.fromJson(dataCollection);
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance.recordError(error, stackTrace,
          reason:
              'Failed fetch employee details (Response code:${response.statusCode})');
      return null;
    }
  }

  Future<List<EmployeeData>?> getEmployeeDataTest() async {
    var response = await client
        .get(Uri.parse(AuthApiConfiguration.instance.employeeListEndpoint));

    try {
      if (response.statusCode != 200) {
        return null;
      }

      final decoded = jsonDecode(response.body);

      return decoded is List
          ? decoded.map((json) => EmployeeData.fromJson(json)).toList()
          : [];
    } catch (error) {
      return null;
    }
  }

  Future<EmployeeData?> getEmployeeDetailsTest(String employeeId) async {
    var response = await client.get(Uri.parse(
        '${AuthApiConfiguration.instance.employeeListEndpoint}/$employeeId'));
    try {
      final dataCollection = jsonDecode(response.body);
      return EmployeeData.fromJson(dataCollection);
    } catch (error) {
      return null;
    }
  }
}
