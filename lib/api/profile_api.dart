import 'dart:convert';

import 'package:employeeviewer/api/auth/api_configuration.dart';
import 'package:employeeviewer/api/auth/auth_api.dart';
import 'package:employeeviewer/api/model/api_models.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:http/http.dart' as http;

class ProfileApi {
  ProfileApi(this.client);

  final http.Client client;

  Future<PersonalData?> getData() async {
    final AuthApi auth = AuthApi();
    final headers = await auth.buildHeaders();
    var response = await client.get(
        Uri.parse(AuthApiConfiguration.instance.personalInfoEndpoint),
        headers: headers);

    try {
      FirebaseCrashlytics.instance.setUserIdentifier(
          PersonalData.fromJson(jsonDecode(response.body)).email);
      FirebaseCrashlytics.instance
          .log('Profile Data|INFO|${DateTime.now().toString()}:\n'
              '{\nResponse code:${response.statusCode}\n}');
      return PersonalData.fromJson(jsonDecode(response.body));
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance.recordError(error, stackTrace,
          reason:
              'Failed fetch profile data(Response code:${response.statusCode})');
      return null;
    }
  }

  Future<PersonalData?> getDataTest() async {
    var response = await client
        .get(Uri.parse(AuthApiConfiguration.instance.personalInfoEndpoint));
    try {
      return PersonalData.fromJson(jsonDecode(response.body));
    } catch (error) {
      print(error);
      return null;
    }
  }
}
