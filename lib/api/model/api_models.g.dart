// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_models.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PersonalDataAdapter extends TypeAdapter<PersonalData> {
  @override
  final int typeId = 2;

  @override
  PersonalData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PersonalData(
      email: fields[0] as String,
      picture: fields[2] as String,
      name: fields[1] as String,
      sub: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, PersonalData obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.email)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.picture)
      ..writeByte(3)
      ..write(obj.sub);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PersonalDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class EmployeeDataAdapter extends TypeAdapter<EmployeeData> {
  @override
  final int typeId = 1;

  @override
  EmployeeData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return EmployeeData(
      id: fields[0] as String,
      email: fields[2] as String,
      fullName: fields[1] as String,
      active: fields[3] as bool,
      employment: fields[4] as String,
      paymentMode: fields[5] as String,
      location: fields[6] as String?,
      peManagement: fields[7] as String,
      paidVacationFrom: fields[8] as String,
      startDate: fields[9] as String,
      terminationDate: fields[10] as String,
      intHours: fields[11] as String,
      seniority: fields[12] as String?,
      senioritySublevel: fields[13] as String?,
      profile: fields[14] as String?,
      position: fields[15] as String?,
      phone: fields[16] as String?,
      photoSmall: fields[17] as String?,
      photoMedium: fields[18] as String?,
      skype: fields[19] as String,
      photo: fields[20] as String?,
      dismissal: fields[21] as String,
      birthday: fields[22] as String,
      manager: fields[23] as String?,
      mentor: fields[24] as String?,
      yearHired: fields[25] as String?,
      yearDismissed: fields[26] as String,
      roomNumber: fields[27] as String,
      englishLevel: fields[28] as String?,
      careerStart: fields[29] as String,
      certificates: fields[30] as String?,
      keInfo: fields[31] as String,
      expectedPromotion: fields[32] as String,
      projects: (fields[33] as List?)?.cast<dynamic>(),
      slackUserId: fields[34] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, EmployeeData obj) {
    writer
      ..writeByte(35)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.fullName)
      ..writeByte(2)
      ..write(obj.email)
      ..writeByte(3)
      ..write(obj.active)
      ..writeByte(4)
      ..write(obj.employment)
      ..writeByte(5)
      ..write(obj.paymentMode)
      ..writeByte(6)
      ..write(obj.location)
      ..writeByte(7)
      ..write(obj.peManagement)
      ..writeByte(8)
      ..write(obj.paidVacationFrom)
      ..writeByte(9)
      ..write(obj.startDate)
      ..writeByte(10)
      ..write(obj.terminationDate)
      ..writeByte(11)
      ..write(obj.intHours)
      ..writeByte(12)
      ..write(obj.seniority)
      ..writeByte(13)
      ..write(obj.senioritySublevel)
      ..writeByte(14)
      ..write(obj.profile)
      ..writeByte(15)
      ..write(obj.position)
      ..writeByte(16)
      ..write(obj.phone)
      ..writeByte(17)
      ..write(obj.photoSmall)
      ..writeByte(18)
      ..write(obj.photoMedium)
      ..writeByte(19)
      ..write(obj.skype)
      ..writeByte(20)
      ..write(obj.photo)
      ..writeByte(21)
      ..write(obj.dismissal)
      ..writeByte(22)
      ..write(obj.birthday)
      ..writeByte(23)
      ..write(obj.manager)
      ..writeByte(24)
      ..write(obj.mentor)
      ..writeByte(25)
      ..write(obj.yearHired)
      ..writeByte(26)
      ..write(obj.yearDismissed)
      ..writeByte(27)
      ..write(obj.roomNumber)
      ..writeByte(28)
      ..write(obj.englishLevel)
      ..writeByte(29)
      ..write(obj.careerStart)
      ..writeByte(30)
      ..write(obj.certificates)
      ..writeByte(31)
      ..write(obj.keInfo)
      ..writeByte(32)
      ..write(obj.expectedPromotion)
      ..writeByte(33)
      ..write(obj.projects)
      ..writeByte(34)
      ..write(obj.slackUserId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EmployeeDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
