import 'package:hive/hive.dart';

part 'api_models.g.dart';

class CorporateRulesTitle {
  CorporateRulesTitle({
    required this.urlList,
    required this.topic,
  });

  final List<CorporateRulesData> urlList;
  final String topic;
}

class CorporateRulesData {
  CorporateRulesData({
    required this.title,
    required this.url,
  });

  final String title;
  final String url;
}

@HiveType(typeId: 2)
class PersonalData extends HiveObject {
  PersonalData({
    required this.email,
    required this.picture,
    required this.name,
    this.sub,
  });

  factory PersonalData.fromJson(Map<String, dynamic> json) {
    return PersonalData(
      email: json['email'],
      picture: json['picture'],
      name: json['name'],
      sub: json['sub'],
    );
  }

  @HiveField(0)
  final String email;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final String picture;
  @HiveField(3)
  final String? sub;

  @override
  String toString() {
    return 'Employee: {name: $name, email: $email, sub: $sub, picture: $picture}';
  }
}

@HiveType(typeId: 1)
class EmployeeData extends HiveObject {
  EmployeeData({
    required this.id,
    required this.email,
    required this.fullName,
    required this.active,
    required this.employment,
    required this.paymentMode,
    required this.location,
    required this.peManagement,
    required this.paidVacationFrom,
    required this.startDate,
    required this.terminationDate,
    required this.intHours,
    required this.seniority,
    required this.senioritySublevel,
    required this.profile,
    required this.position,
    required this.phone,
    required this.photoSmall,
    required this.photoMedium,
    required this.skype,
    required this.photo,
    required this.dismissal,
    required this.birthday,
    required this.manager,
    required this.mentor,
    required this.yearHired,
    required this.yearDismissed,
    required this.roomNumber,
    required this.englishLevel,
    required this.careerStart,
    required this.certificates,
    required this.keInfo,
    required this.expectedPromotion,
    required this.projects,
    required this.slackUserId,
  });

  factory EmployeeData.fromJson(Map<String, dynamic> json) {
    return EmployeeData(
      id: json['id'].toString(),
      email: json['email'].toString(),
      fullName: json['fullName'].toString(),
      active: json['active'],
      employment: json['employment'].toString(),
      paymentMode: json['paymentMode'].toString(),
      location: json['location'].toString(),
      peManagement: json['peManagement'].toString(),
      paidVacationFrom: json['paidVacationFrom'].toString(),
      startDate: json['startDate'].toString(),
      terminationDate: json['terminationDate'].toString(),
      intHours: json['intHours'].toString(),
      seniority: json['seniority'],
      senioritySublevel: json['senioritySublevel'],
      profile: json['profile'],
      position: json['position'],
      phone: json['phone'],
      photoSmall: json['photoSmall'],
      photoMedium: json['photoMedium'],
      skype: json['skype'].toString(),
      photo: json['photo'],
      dismissal: json['dismissal'].toString(),
      birthday: json['birthday'].toString(),
      manager: json['manager'],
      mentor: json['mentor'],
      yearHired: json['yearHired'].toString(),
      yearDismissed: json['yearDismissed'].toString(),
      roomNumber: json['roomNumber'].toString(),
      englishLevel: json['englishLevel'],
      careerStart: json['careerStart'].toString(),
      certificates: json['certificates'],
      keInfo: json['keInfo'].toString(),
      expectedPromotion: json['expectedPromotion'].toString(),
      projects: json['projects'],
      slackUserId: json['slackUserId'],
    );
  }

  @HiveField(0)
  final String id;
  @HiveField(1)
  final String fullName;
  @HiveField(2)
  final String email;
  @HiveField(3)
  final bool active;
  @HiveField(4)
  final String employment;
  @HiveField(5)
  final String paymentMode;
  @HiveField(6)
  final String? location;
  @HiveField(7)
  final String peManagement;
  @HiveField(8)
  final String paidVacationFrom;
  @HiveField(9)
  final String startDate;
  @HiveField(10)
  final String terminationDate;
  @HiveField(11)
  final String intHours;
  @HiveField(12)
  final String? seniority;
  @HiveField(13)
  final String? senioritySublevel;
  @HiveField(14)
  final String? profile;
  @HiveField(15)
  final String? position;
  @HiveField(16)
  final String? phone;
  @HiveField(17)
  final String? photoSmall;
  @HiveField(18)
  final String? photoMedium;
  @HiveField(19)
  final String skype;
  @HiveField(20)
  final String? photo;
  @HiveField(21)
  final String dismissal;
  @HiveField(22)
  final String birthday;
  @HiveField(23)
  final String? manager;
  @HiveField(24)
  final String? mentor;
  @HiveField(25)
  final String? yearHired;
  @HiveField(26)
  final String yearDismissed;
  @HiveField(27)
  final String roomNumber;
  @HiveField(28)
  final String? englishLevel;
  @HiveField(29)
  final String careerStart;
  @HiveField(30)
  final String? certificates;
  @HiveField(31)
  final String keInfo;
  @HiveField(32)
  final String expectedPromotion;
  @HiveField(33)
  final List? projects;
  @HiveField(34)
  final String? slackUserId;
}
