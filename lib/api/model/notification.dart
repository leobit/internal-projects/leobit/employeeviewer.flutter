import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

part 'notification.g.dart';

@HiveType(typeId: 3)
class NotificationUpdate extends HiveObject {
  NotificationUpdate({
    required this.isChecked,
    required this.id,
    required this.fullName,
    this.oldData,
    this.newData,
    this.photoSmall,
    this.notificationType,
  });

  @HiveField(0)
  final String id;
  @HiveField(1)
  final String fullName;
  @HiveField(2)
  final bool isChecked;
  @HiveField(3)
  final EmployeeData? oldData;
  @HiveField(4)
  final EmployeeData? newData;
  @HiveField(5)
  final String? photoSmall;
  @HiveField(6)
  final String? notificationType;

  List<TextSpan> buildNewcomerNotification() {
    return [
      const TextSpan(text: 'Say hi for our new friend '),
      TextSpan(
          text: fullName,
          style: TextStyle(color: AppColors.leoColor.withOpacity(0.8))),
      const TextSpan(text: ' 🎉🎉🎉')
    ];
  }

  List<TextSpan> buildLeaveNotification() {
    return [
      TextSpan(
          text: fullName,
          style: TextStyle(color: AppColors.leoColor.withOpacity(0.8))),
      const TextSpan(text: ' went for bread, but promised to return 😢😢😢')
    ];
  }

  List<TextSpan> buildSeniorityNotification() {
    return [
      TextSpan(
          text: fullName,
          style: TextStyle(color: AppColors.leoColor.withOpacity(0.8))),
      TextSpan(
          text: (oldData!.seniority == null)
              ? ' has been promoted to ${newData!.seniority}. Congrats!'
              : ' has been promoted from ${oldData!.seniority} to ${newData!.seniority}. Congrats!')
    ];
  }

  List<TextSpan> buildCertificateNotification() {
    final String certificate = (newData!.certificates ?? '')
        .split('\n')
        .toList()
        .where((element) => !(oldData!.certificates ?? '')
            .split('\n')
            .toList()
            .contains(element))
        .toList()
        .join(', ');

    return [
      TextSpan(
          text: fullName,
          style: TextStyle(color: AppColors.leoColor.withOpacity(0.8))),
      TextSpan(text: ' have successfully acquired $certificate !')
    ];
  }
}
