// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NotificationUpdateAdapter extends TypeAdapter<NotificationUpdate> {
  @override
  final int typeId = 3;

  @override
  NotificationUpdate read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return NotificationUpdate(
      isChecked: fields[2] as bool,
      id: fields[0] as String,
      fullName: fields[1] as String,
      oldData: fields[3] as EmployeeData?,
      newData: fields[4] as EmployeeData?,
      photoSmall: fields[5] as String?,
      notificationType: fields[6] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, NotificationUpdate obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.fullName)
      ..writeByte(2)
      ..write(obj.isChecked)
      ..writeByte(3)
      ..write(obj.oldData)
      ..writeByte(4)
      ..write(obj.newData)
      ..writeByte(5)
      ..write(obj.photoSmall)
      ..writeByte(6)
      ..write(obj.notificationType);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NotificationUpdateAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
