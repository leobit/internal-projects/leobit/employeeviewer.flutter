class StatisticModel {
  StatisticModel({
    required this.projectManagerList,
    required this.totalExperienceList,
    required this.yearHiredList,
    required this.profileList,
    required this.seniorityList,
    required this.certificateList,
    required this.projectList,
  });

  final List<BarStatistic> yearHiredList;
  final List<BarStatistic> totalExperienceList;
  final List<BarStatistic> projectManagerList;
  final List<BarStatistic> profileList;
  final List<BarStatistic> seniorityList;
  final List<BarStatistic> certificateList;
  final List<BarStatistic> projectList;
}

class BarStatistic {
  BarStatistic({required this.key, required this.count});

  final String key;
  final int count;
}
