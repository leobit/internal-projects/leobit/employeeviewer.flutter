import 'package:flutter/foundation.dart';

class AuthApiConfiguration {
  const AuthApiConfiguration._({
    required this.scopes,
    required this.tokenEndpoint,
    required this.identifier,
    required this.authEndpoint,
    required this.employeeListEndpoint,
    required this.logoutEndpoint,
    required this.personalInfoEndpoint,
    required this.redirectEndpoint,
    required this.expConst,
    required this.allowedHosts,
  });

  final String tokenEndpoint;
  final List<String> scopes;
  final String identifier;
  final String authEndpoint;
  final String redirectEndpoint;
  final String personalInfoEndpoint;
  final String employeeListEndpoint;
  final String logoutEndpoint;
  final int expConst;
  final List<String> allowedHosts;

  static AuthApiConfiguration? _instance;

  static AuthApiConfiguration get instance {
    if (_instance == null) {
      final String tokenEndpoint;
      final String authEndpoint;
      final String logoutEndpoint;
      final String personalInfoEndpoint;
      final allowedHosts = ['api.employee.leobit.co', 'employee.leobit.co'];

      if (kReleaseMode) {
        tokenEndpoint = 'https://identity.leobit.co/connect/token';
        authEndpoint = 'https://identity.leobit.co/connect/authorize';
        logoutEndpoint = 'https://identity.leobit.co/connect/endsession';
        personalInfoEndpoint = 'https://identity.leobit.co/connect/userinfo';
        allowedHosts.add('identity.leobit.co');
      } else {
        tokenEndpoint = 'https://test.identity.leobit.co/connect/token';
        authEndpoint = 'https://test.identity.leobit.co/connect/authorize';
        logoutEndpoint = 'https://test.identity.leobit.co/connect/endsession';
        personalInfoEndpoint =
            'https://test.identity.leobit.co/connect/userinfo';
        allowedHosts.add('test.identity.leobit.co');
      }

      _instance = AuthApiConfiguration._(
        scopes: ['openid', 'profile', 'employee', 'offline_access'],
        identifier: 'leobit.employee.flutter',
        tokenEndpoint: tokenEndpoint,
        authEndpoint: authEndpoint,
        employeeListEndpoint: 'https://api.employee.leobit.co/employee',
        logoutEndpoint: logoutEndpoint,
        personalInfoEndpoint: personalInfoEndpoint,
        redirectEndpoint: 'co.leobit.employeeapp://auth',
        expConst: 300000,
        allowedHosts: allowedHosts,
      );
    }

    return _instance!;
  }
}
