import 'dart:async';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'api_configuration.dart';

class AuthApi {
  final FlutterAppAuth _appAuth = const FlutterAppAuth();
  final _storage = const FlutterSecureStorage();
  static Future<void>? _refreshTokenFuture;

  Future<void> _saveCredToStorage(String? accessToken, String? refreshToken,
      String? idToken, int? exp) async {
    _storage.write(key: 'access_token', value: accessToken);
    _storage.write(key: 'refresh_token', value: refreshToken);
    _storage.write(key: 'id_token', value: idToken);
    _storage.write(key: 'exp', value: exp.toString());
  }

  Future<void> _clearCredFromStorage() async {
    _storage.delete(key: 'favorite_list');
    _storage.delete(key: 'access_token');
    _storage.delete(key: 'refresh_token');
    _storage.delete(key: 'id_token');
    _storage.delete(key: 'exp');
  }

  Future authUser(BuildContext context) async {
    try {
      final AuthorizationTokenResponse? result =
          await _appAuth.authorizeAndExchangeCode(AuthorizationTokenRequest(
              AuthApiConfiguration.instance.identifier,
              AuthApiConfiguration.instance.redirectEndpoint,
              scopes: AuthApiConfiguration.instance.scopes,
              serviceConfiguration: AuthorizationServiceConfiguration(
                  authorizationEndpoint:
                      AuthApiConfiguration.instance.authEndpoint,
                  tokenEndpoint: AuthApiConfiguration.instance.tokenEndpoint)));
      FirebaseCrashlytics.instance
          .log('Auth user|INFO|${DateTime.now().toString()}:\n'
              '{\nAccessToken: ${result?.accessToken}\n'
              'RefreshToken: ${result?.refreshToken}\n}');

      await _saveCredToStorage(
              result?.accessToken,
              result?.refreshToken,
              result?.idToken,
              result?.accessTokenExpirationDateTime?.millisecondsSinceEpoch)
          .then((value) => Navigator.of(context).pushNamed('/list'));
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance.recordError(error, stackTrace,
          reason: 'Failed login user & get accessToken');
      return error;
    }
  }

  Future<void> refreshToken() {
    _refreshTokenFuture ??=
        _refreshToken().whenComplete(() => _refreshTokenFuture = null);

    return _refreshTokenFuture!;
  }

  Future<void> _refreshToken() async {
    try {
      final refreshToken = await _storage.read(key: 'refresh_token');
      final TokenResponse? response = await _appAuth.token(TokenRequest(
        AuthApiConfiguration.instance.identifier,
        AuthApiConfiguration.instance.redirectEndpoint,
        serviceConfiguration: AuthorizationServiceConfiguration(
            authorizationEndpoint: AuthApiConfiguration.instance.authEndpoint,
            tokenEndpoint: AuthApiConfiguration.instance.tokenEndpoint),
        refreshToken: refreshToken,
      ));
      FirebaseCrashlytics.instance
          .log('Refresh Token|INFO|${DateTime.now().toString()}:\n'
              '{\nAccessToken: ${response?.accessToken}\n'
              'RefreshToken: ${response?.refreshToken}\n}');

      await _saveCredToStorage(
          response?.accessToken,
          response?.refreshToken,
          response?.idToken,
          response?.accessTokenExpirationDateTime?.millisecondsSinceEpoch);
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance
          .recordError(error, stackTrace, reason: 'Failed refresh token');
    }
  }

  Future<Map<String, String>> buildHeaders() async {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${await getAccessToken()}',
    };
  }

  Future<String?> getAccessToken() async {
    final exp = int.parse(await getExpTime() as String);
    final int timeDifferences = exp - DateTime.now().millisecondsSinceEpoch;
    if (timeDifferences < AuthApiConfiguration.instance.expConst * 0.5) {
      FirebaseCrashlytics.instance
          .log('EXP Token Time|INFO|${DateTime.now().toString()}:\n'
              '{\nCurrent time differences:${timeDifferences.toString()}\n'
              'Starting refreshing token\n}');

      await refreshToken();
    } else {
      FirebaseCrashlytics.instance
          .log('EXP Token Time|INFO|${DateTime.now().toString()}:\n'
              '{\nCurrent time differences:${timeDifferences.toString()}\n'
              'Token are valid\n}');
    }
    return await _storage.read(key: 'access_token');
  }

  Future<String?> getExpTime() async {
    return await _storage.read(key: 'exp');
  }

  Future<void> logoutUser() async {
    await _appAuth.endSession(EndSessionRequest(
        idTokenHint: await _storage.read(key: 'id_token'),
        postLogoutRedirectUrl: AuthApiConfiguration.instance.redirectEndpoint,
        serviceConfiguration: AuthorizationServiceConfiguration(
            authorizationEndpoint: AuthApiConfiguration.instance.authEndpoint,
            tokenEndpoint: AuthApiConfiguration.instance.tokenEndpoint,
            endSessionEndpoint: AuthApiConfiguration.instance.logoutEndpoint)));
  }

  Future<void> clearStorage() async {
    return await _clearCredFromStorage();
  }
}
