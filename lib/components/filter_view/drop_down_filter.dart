import 'dart:io' show Platform;

import 'package:dropdown_search/dropdown_search.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class DropDownFilter extends StatelessWidget {
  const DropDownFilter({
    super.key,
    required this.hint,
    required this.optionList,
    required this.isSeparated,
  });

  final String hint;
  final List<String> optionList;
  final bool isSeparated;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) => SizedBox(
          child: Platform.isIOS
              ? _iOSDropdownFilter(context, store, state)
              : _androidDropdownFilter(context, store, state)),
    );
  }

  Widget _androidDropdownFilter(
      BuildContext context, Store<AppState> store, AppState state) {
    return Container(
        height: 55,
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
        child: DropdownSearch(
          popupProps: PopupProps<String>.menu(
            menuProps: MenuProps(
              backgroundColor: Theme.of(context).primaryColor,
            ),
            constraints: const BoxConstraints(maxHeight: 160),
          ),
          dropdownDecoratorProps: DropDownDecoratorProps(
            dropdownSearchDecoration: InputDecoration(
              labelText: hint,
              contentPadding: const EdgeInsets.fromLTRB(12, 12, 0, 0),
              border: const OutlineInputBorder(),
            ),
          ),
          selectedItem: state.filterValue[hint],
          items: optionList,
          onChanged: (value) {
            store.dispatch(UpdateFilterValueAction(hint, value));
          },
        ));
  }

  Widget _iOSDropdownFilter(
      BuildContext context, Store<AppState> store, AppState state) {
    String currentValue = optionList.first;

    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: (isSeparated)
            ? const Border(
                bottom:
                    BorderSide(color: CupertinoColors.systemGrey, width: 0.75),
              )
            : null,
      ),
      child: ListTile(
        onTap: () => showCupertinoModalPopup(
            context: context,
            builder: (context) => WillPopScope(
                  onWillPop: () async {
                    store.dispatch(UpdateFilterValueAction(hint, currentValue));
                    return true;
                  },
                  child: CupertinoActionSheet(
                    cancelButton: CupertinoActionSheetAction(
                      onPressed: () async {
                        store.dispatch(
                            UpdateFilterValueAction(hint, currentValue));
                        Navigator.of(context).pop();
                      },
                      child: const Text('Cancel'),
                    ),
                    actions: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.3,
                        child: CupertinoPicker(
                            itemExtent: 35,
                            onSelectedItemChanged: (value) {
                              currentValue = optionList[value];
                            },
                            children: optionList
                                .map((e) => Text(e,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .textTheme
                                            .titleLarge
                                            ?.color)))
                                .toList()),
                      )
                    ],
                  ),
                )),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(hint),
            Text(state.filterValue[hint]),
          ],
        ),
        trailing: const Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}
