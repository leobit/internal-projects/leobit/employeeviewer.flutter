import 'dart:io' show Platform;

import 'package:employeeviewer/components/filter_view/drop_down_filter.dart';
import 'package:employeeviewer/components/filter_view/drop_down_search.dart';
import 'package:employeeviewer/components/filter_view/text_field.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class FilterSection extends StatelessWidget {
  const FilterSection({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 6),
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(12),
              decoration: Platform.isIOS
                  ? BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(
                          color: Theme.of(context).colorScheme.background))
                  : null,
              child: const Column(
                children: [
                  CustomTextField(hint: 'Name', isNumField: false),
                  CustomTextField(hint: 'Email', isNumField: false),
                  CustomTextField(hint: 'Phone', isNumField: true),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.all(12),
              decoration: Platform.isIOS
                  ? BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(
                          color: Theme.of(context).colorScheme.background))
                  : null,
              child: const Column(
                children: [
                  DropDownFilter(
                      hint: 'Status',
                      optionList: ['Active', 'Inactive', 'All'],
                      isSeparated: true),
                  DropDownFilter(
                    hint: 'Employment',
                    optionList: ['FT', 'T&M', 'All'],
                    isSeparated: false,
                  ),
                ],
              ),
            ),
            StoreConnector<AppState, AppState>(
              converter: (store) => store.state,
              builder: (context, state) {
                final dropDownValues = state.dropDownValue.keys.toList();

                return Container(
                  margin: const EdgeInsets.all(12),
                  decoration: Platform.isIOS
                      ? BoxDecoration(
                          color: Theme.of(context).colorScheme.background,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8.0)),
                          border: Border.all(
                              color: Theme.of(context).colorScheme.background))
                      : null,
                  child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) => CustomDropdownSearch(
                            hint: dropDownValues[index],
                          ),
                      separatorBuilder: (context, index) => Divider(
                            color: Platform.isIOS
                                ? CupertinoColors.systemGrey
                                : Colors.transparent,
                          ),
                      itemCount: dropDownValues.length),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
