import 'dart:io';

import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    super.key,
    required this.hint,
    required this.isNumField,
  });

  final String hint;
  final bool isNumField;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 55,
        color: Colors.transparent,
        margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
        child: Platform.isIOS
            ? _iOSTextField(isNumField, store, context)
            : _androidTextField(context, isNumField, store));
  }

  TextField _androidTextField(
      BuildContext context, bool isNumField, Store<AppState> store) {
    return isNumField
        ? TextField(
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
            ],
            controller:
                TextEditingController(text: store.state.filterValue[hint]),
            onChanged: (value) =>
                store.dispatch(UpdateFilterValueAction(hint, value)),
            decoration: InputDecoration(
                hintText: hint,
                focusColor: Theme.of(context).primaryColor,
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(8),
                )),
          )
        : TextField(
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z @]')),
            ],
            controller:
                TextEditingController(text: store.state.filterValue[hint]),
            onChanged: (value) =>
                store.dispatch(UpdateFilterValueAction(hint, value)),
            decoration: InputDecoration(
                hintText: hint,
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(8),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(8),
                )),
          );
  }

  CupertinoTextField _iOSTextField(
      bool isNumField, Store<AppState> store, BuildContext context) {
    return isNumField
        ? CupertinoTextField(
            decoration: const BoxDecoration(
              color: Colors.transparent,
            ),
            cursorColor: CupertinoColors.white,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
            ],
            style: TextStyle(
                color: Theme.of(context).textTheme.headlineSmall?.color),
            controller:
                TextEditingController(text: store.state.filterValue[hint]),
            onChanged: (value) =>
                store.dispatch(UpdateFilterValueAction(hint, value)),
            placeholder: hint,
            placeholderStyle: TextStyle(
                color: Theme.of(context).textTheme.headlineSmall?.color),
          )
        : CupertinoTextField(
            decoration: const BoxDecoration(
              color: Colors.transparent,
              border: Border(
                bottom:
                    BorderSide(color: CupertinoColors.systemGrey, width: 0.75),
              ),
            ),
            style: TextStyle(
                color: Theme.of(context).textTheme.headlineSmall?.color),
            placeholderStyle: TextStyle(
                color: Theme.of(context).textTheme.headlineSmall?.color),
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z @]')),
            ],
            controller:
                TextEditingController(text: store.state.filterValue[hint]),
            onChanged: (value) =>
                store.dispatch(UpdateFilterValueAction(hint, value)),
            placeholder: hint,
          );
  }
}
