import 'dart:io' show Platform;

import 'package:employeeviewer/components/filter_view/filter_section.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class FilterView extends StatelessWidget {
  const FilterView({super.key});

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);
    return SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
            appBar: (Platform.isIOS
                ? CupertinoNavigationBar(
                    backgroundColor: Theme.of(context).primaryColor,
                    middle: Text(
                      'Filter',
                      style: Theme.of(context).textTheme.displayLarge,
                    ),
                    trailing: InkWell(
                      onTap: () {
                        store.dispatch(ClearFilerAction());
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        'Reset',
                        style: TextStyle(fontSize: 20, color: Colors.red),
                      ),
                    ),
                    leading: InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(
                              'The number of filtered Employee: ${store.state.filteredEmployeeList.length}'),
                        ));
                      },
                      child: const Row(
                        children: [
                          Icon(Icons.arrow_back_ios, color: Colors.grey),
                          Text(
                            'Back',
                            style: TextStyle(fontSize: 20, color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                  )
                : AppBar(
                    backgroundColor: Theme.of(context).primaryColor,
                    automaticallyImplyLeading: false,
                    scrolledUnderElevation: 0.0,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const SettingsAppBar(title: 'Advanced Search'),
                        InkWell(
                          onTap: () {
                            store.dispatch(ClearFilerAction());
                            Navigator.of(context).pop();
                          },
                          child: const Text(
                            'Reset',
                            style: TextStyle(fontSize: 20, color: Colors.grey),
                          ),
                        ),
                      ],
                    ))) as PreferredSizeWidget,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            drawerEnableOpenDragGesture: false,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            bottomNavigationBar: const BottomNavigation(currentIndex: 0),
            body: const FilterSection()));
  }
}
