import 'dart:io' show Platform;

import 'package:dropdown_search/dropdown_search.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/selectors/employee_list_selector.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:redux/redux.dart';

class CustomDropdownSearch extends StatelessWidget {
  const CustomDropdownSearch({super.key, required this.hint});

  final String hint;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) => Container(
          width: MediaQuery.of(context).size.width,
          height: Platform.isIOS ? 55.0 : null,
          margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
          child: Platform.isIOS
              ? _iOSDropdownFilter(context, store)
              : _androidDropdownFilter(context, store)),
    );
  }

  Widget _androidDropdownFilter(BuildContext context, Store<AppState> store) {
    var values = List.from(store.state.filterValue[hint]);

    return DropdownSearch.multiSelection(
      popupProps: PopupPropsMultiSelection.modalBottomSheet(
        showSearchBox: true,
        modalBottomSheetProps: ModalBottomSheetProps(
          backgroundColor: Theme.of(context).primaryColor,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
        constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height,
        ),
        searchFieldProps: TextFieldProps(
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            contentPadding: const EdgeInsets.fromLTRB(12, 12, 8, 0),
            labelText: 'Search a ${hint.toLowerCase()}',
          ),
        ),
        onItemAdded: (selectedItems, addedItem) => values = selectedItems,
        onItemRemoved: (selectedItems, removedItem) => values = selectedItems,
        validationWidgetBuilder: (context, item) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(96, 178, 58, 0.8),
                  ),
                  onPressed: () {
                    store.dispatch(UpdateFilterValueAction(hint, values));
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    'OK',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          );
        },
        selectionWidget: (context, item, isSelected) {
          return Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: (isSelected)
                ? const Icon(
                    Icons.check_box,
                    color: Color.fromRGBO(96, 178, 58, 0.8),
                  )
                : const Icon(
                    Icons.check_box_outline_blank,
                    color: Colors.grey,
                  ),
          );
        },
      ),
      dropdownDecoratorProps: DropDownDecoratorProps(
        dropdownSearchDecoration: InputDecoration(
          labelText: hint,
          contentPadding: const EdgeInsets.fromLTRB(12, 12, 0, 0),
          border: const OutlineInputBorder(),
        ),
      ),
      selectedItems: store.state.filterValue[hint] as List,
      items: store.state.dropDownValue[hint] ?? [],
      dropdownBuilder: (context, selectedItems) {
        Widget buildItem(String item) => Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(
                  child: Container(
                    padding: const EdgeInsets.all(6),
                    margin: const EdgeInsets.all(1),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.leoColor.withOpacity(0.75)),
                    child: Text(
                      item,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            );

        return Wrap(
          children: selectedItems
              .map((element) => buildItem(element.toString()))
              .toList(),
        );
      },
    );
  }

  Widget _iOSDropdownFilter(BuildContext context, Store<AppState> store) {
    return Container(
      color: Colors.transparent,
      child: ListTile(
        onTap: () {
          store.dispatch(PrepareMultiselectFilter());
          showCupertinoModalBottomSheet(
            context: context,
            builder: (context) => Scaffold(
                appBar: CupertinoNavigationBar(
                  backgroundColor: Theme.of(context).primaryColor,
                  leading: InkWell(
                    onTap: () => Navigator.of(context).pop(),
                    child: const Row(
                      children: [
                        Icon(Icons.arrow_back_ios,
                            color: CupertinoColors.activeBlue),
                        Text(
                          'Back',
                          style: TextStyle(
                              fontSize: 20, color: CupertinoColors.activeBlue),
                        ),
                      ],
                    ),
                  ),
                  middle: Text(
                    hint,
                    style: TextStyle(
                        fontSize:
                            Theme.of(context).textTheme.displayLarge?.fontSize,
                        color: Theme.of(context)
                            .textTheme
                            .displayLarge
                            ?.color
                            ?.withOpacity(1)),
                  ),
                ),
                body: SingleChildScrollView(
                  child: StoreConnector<AppState, List<String?>>(
                    converter: (store) => filteredMultiselectListSelector(
                        store.state,
                        store.state.dropDownValue[hint] as List<String?>),
                    builder: (context, filteredList) {
                      var dataList = ((store.state.multiselectFilterValue == '')
                          ? store.state.dropDownValue[hint]!
                          : filteredList);
                      return Column(children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          padding: const EdgeInsets.all(4),
                          child: CupertinoTextField(
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.background,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0)),
                                border: Border.all(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .background)),
                            placeholderStyle: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .headlineSmall
                                    ?.color),
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(
                                  RegExp('[a-zA-Z]')),
                            ],
                            onChanged: (value) => store.dispatch(
                                UpdateMultiselectFilterList(
                                    store.state.dropDownValue[hint]!, value)),
                            placeholder: 'Search',
                            prefix: Container(
                                margin: const EdgeInsets.only(left: 8),
                                child: const Icon(
                                  Icons.search_sharp,
                                  color: CupertinoColors.systemGrey,
                                )),
                          ),
                        ),
                        Container(
                            margin: const EdgeInsets.all(8),
                            padding: const EdgeInsets.all(4),
                            height: MediaQuery.of(context).size.height,
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.background,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0)),
                                border: Border.all(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .background)),
                            child: ListView.separated(
                                itemCount: dataList.length,
                                separatorBuilder: (context, index) =>
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0),
                                      child: Divider(
                                        color: CupertinoColors.systemGrey,
                                      ),
                                    ),
                                itemBuilder: (context, index) => InkWell(
                                    onTap: () => store.dispatch(
                                        UpdateModalityFilterValueAction(
                                            hint, dataList[index])),
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      height: 44,
                                      child: Center(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            dataList[index]!,
                                            style:
                                                const TextStyle(fontSize: 17),
                                          ),
                                          Icon(CupertinoIcons.checkmark_alt,
                                              color: (store.state
                                                              .filterValue[hint]
                                                          as List)
                                                      .contains(dataList[index])
                                                  ? CupertinoColors.activeBlue
                                                  : Colors.transparent),
                                        ],
                                      )),
                                    ))))
                      ]);
                    },
                  ),
                )),
          );
        },
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(hint),
          ],
        ),
        trailing: const Icon(Icons.keyboard_arrow_right),
      ),
    );
  }
}
