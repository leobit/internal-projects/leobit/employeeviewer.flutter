import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';

class SettingsAppBar extends StatelessWidget {
  const SettingsAppBar({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [
      Text(title,
          style: TextStyle(
              color: AppColors.leoColor.withOpacity(0.8), fontSize: 20))
    ]);
  }
}
