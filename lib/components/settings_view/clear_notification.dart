import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_notification_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ClearNotification extends StatelessWidget {
  const ClearNotification({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreBuilder<AppState>(
      builder: (context, store) => SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ListTile(
            onTap: () {
              store.dispatch(getAllNotifications(
                  NotificationAction.clearStoredNotification));
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text('You clear all your notifications'),
              ));
            },
            leading: const Icon(
              Icons.notifications_off,
              size: 30,
            ),
            title: Text(
              'Clear all notification',
              style: Theme.of(context).textTheme.displayLarge,
            ),
          )),
    );
  }
}
