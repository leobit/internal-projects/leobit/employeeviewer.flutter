import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class NightMode extends StatelessWidget {
  const NightMode({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) {
        return SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ListTile(
            leading: const Icon(
              Icons.swap_horizontal_circle_outlined,
              size: 30,
            ),
            title: Text(
              'Night mode',
              style: Theme.of(context).textTheme.displayLarge,
            ),
            trailing: Switch(
              value: state.themeState,
              onChanged: (value) {
                StoreProvider.of<AppState>(context)
                    .dispatch(updateThemeState(value));
              },
              activeTrackColor: AppColors.lightGreen,
              activeColor: AppColors.leoColor.withOpacity(0.8),
            ),
          ),
        );
      },
    );
  }
}
