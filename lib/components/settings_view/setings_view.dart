import 'dart:io' show Platform;

import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/clear_notification.dart';
import 'package:employeeviewer/components/settings_view/night_mode.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (locate) {
        if (locate.delta.dx > 0) {
          Navigator.of(context).pop();
        }
      },
      child: SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
            bottomNavigationBar: const BottomNavigation(currentIndex: 4),
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Theme.of(context).primaryColor,
                title: Platform.isIOS
                    ? const BackNavigationAppbar(
                        title: 'Settings',
                      )
                    : const SettingsAppBar(
                        key: Key('settings_appBar'),
                        title: 'Settings',
                      )),
            body: Container(
                margin: const EdgeInsets.symmetric(vertical: 15),
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  SingleChildScrollView(
                      child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(4),
                    decoration: Platform.isIOS
                        ? BoxDecoration(
                            color: Theme.of(context).colorScheme.background,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10.0)),
                            border: Border.all(
                                color:
                                    Theme.of(context).colorScheme.background))
                        : null,
                    child: const Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        NightMode(),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 24.0),
                          child: Divider(
                            color: CupertinoColors.systemGrey,
                          ),
                        ),
                        ClearNotification()
                      ],
                    ),
                  ))
                ]))),
      ),
    );
  }
}
