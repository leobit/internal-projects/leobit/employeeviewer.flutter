import 'dart:io' show Platform;

import 'package:employeeviewer/components/employee_list/employee_list.dart';
import 'package:employeeviewer/components/employee_list/employee_list_appbar.dart';
import 'package:employeeviewer/components/favorite_employee_list/favorite_employee_list.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';

class EmployeeListView extends StatelessWidget {
  const EmployeeListView({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: GestureDetector(
            onPanUpdate: (locate) {
              if (locate.delta.dx < -10) {
                Navigator.of(context).pushNamed('/filter');
              }
            },
            child: SafeArea(
                bottom: false,
                top: false,
                child: Scaffold(
                  key: const Key('employee_list'),
                  bottomNavigationBar: const BottomNavigation(currentIndex: 0),
                  appBar: AppBar(
                      backgroundColor: Theme.of(context).primaryColor,
                      automaticallyImplyLeading: false,
                      scrolledUnderElevation: 0.0,
                      title: const EmployeeListAppBar(),
                      bottom: Platform.isIOS
                          ? null
                          : TabBar(
                              indicatorColor: AppColors.leoColor,
                              labelColor: AppColors.leoColor,
                              labelStyle:
                                  Theme.of(context).textTheme.titleLarge,
                              unselectedLabelColor: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .color!
                                  .withOpacity(0.76),
                              tabs: const [
                                Tab(text: 'All employees'),
                                Tab(text: 'Favorites'),
                              ],
                            )),
                  body: Platform.isIOS
                      ? const EmployeeList()
                      : const TabBarView(
                          children: [EmployeeList(), FavoriteEmployeeList()]),
                ))));
  }
}
