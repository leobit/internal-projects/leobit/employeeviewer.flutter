import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:employeeviewer/redux/selectors/employee_list_selector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import '../../template/colors.dart';

class Employee extends StatelessWidget {
  const Employee({super.key, required this.employeeData});

  final EmployeeData employeeData;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return Platform.isIOS
        ? Padding(
            padding: const EdgeInsets.only(top: 1.5, bottom: 3.0),
            child: CupertinoListTile(
              onTap: () => _onTapEmployee(context, store),
              leading: _employeePhoto(),
              leadingSize: 66.0,
              title: Text(employeeData.fullName),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(_getSenioritySpecialization(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 14.0,
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      employeeData.email,
                      style: const TextStyle(fontSize: 13.0),
                    ),
                  ),
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 1.5),
                child: StoreConnector<AppState, AppState>(
                  converter: (store) => store.state,
                  builder: (context, state) =>
                      Container(child: _favoriteMark(store, employeeData.id)),
                ),
              ),
            ),
          )
        : _androidEmployeeItem(context, store);
  }

  Widget _androidEmployeeItem(BuildContext context, Store<AppState> store) {
    final textColor = Theme.of(context).hintColor.withOpacity(0.6);

    return InkWell(
      onTap: () => _onTapEmployee(context, store),
      child: Container(
        padding: const EdgeInsets.only(
            left: 20.0, right: 14.0, top: 6.0, bottom: 6.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _employeePhoto(),
            const SizedBox(width: 8.0),
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(left: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FittedBox(
                      child: Text(
                        employeeData.fullName,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .color!
                                .withOpacity(0.76)),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    FittedBox(
                      child: Text(
                        _getSenioritySpecialization(),
                        style: TextStyle(
                            fontSize: 14.0, color: textColor, height: 1.34),
                      ),
                    ),
                    Text(
                      employeeData.email,
                      maxLines: 1,
                      style: TextStyle(fontSize: 13, color: textColor),
                      overflow: TextOverflow.ellipsis,
                    )
                  ],
                ),
              ),
            ),
            StoreConnector<AppState, AppState>(
                converter: (store) => store.state,
                builder: (context, state) =>
                    Container(child: _favoriteMark(store, employeeData.id))),
          ],
        ),
      ),
    );
  }

  Widget _employeePhoto() {
    return employeeData.photoSmall != null
        ? CircleAvatar(
            radius: 33.0,
            backgroundImage: const AssetImage('assets/images/leobit.png'),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100.0),
              child: Image.network(employeeData.photoSmall as String),
            ))
        : const CircleAvatar(
            radius: 33.0,
            backgroundImage: AssetImage('assets/images/leobit.png'),
          );
  }

  Container _favoriteMark(Store<AppState> store, String employeeId) {
    return Container(
      margin: const EdgeInsets.all(12),
      child: InkWell(
          onTap: () {
            store.dispatch(toggleFavoriteEmployee(employeeId));
          },
          child: StoreConnector<AppState, bool>(
            converter: (store) =>
                favoriteEmployeeSelector(store.state, employeeId),
            builder: (context, isFavorite) => Icon(
              isFavorite ? (Icons.favorite) : Icons.favorite_border,
              size: 20,
              color:
                  isFavorite ? AppColors.leoColor : CupertinoColors.systemGrey,
            ),
          )),
    );
  }

  String _getSenioritySpecialization() {
    final stringBuffer = StringBuffer();
    if (employeeData.seniority != null) {
      stringBuffer.write(employeeData.seniority);
    }

    if (employeeData.profile != null) {
      if (stringBuffer.isNotEmpty) {
        stringBuffer.write(' - ');
      }

      stringBuffer.write(employeeData.profile);
    }

    if (stringBuffer.isEmpty) {
      stringBuffer.write(' - ');
    }

    return stringBuffer.toString();
  }

  void _onTapEmployee(BuildContext context, Store<AppState> store) {
    store.dispatch(getEmployeeDetails(employeeData.id));
    Navigator.of(context).pushNamed('/details', arguments: employeeData.id);
  }
}
