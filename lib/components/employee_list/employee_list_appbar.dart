import 'dart:io';

import 'package:employeeviewer/components/filter_view/filter_view.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class EmployeeListAppBar extends StatelessWidget {
  const EmployeeListAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = StoreProvider.of<AppState>(context);

    return Platform.isIOS
        ? Stack(children: [
            Align(
              alignment: Alignment.center,
              child: Text(
                'Employee List',
                style: TextStyle(
                    fontSize:
                        Theme.of(context).textTheme.displayLarge?.fontSize,
                    color: Theme.of(context)
                        .textTheme
                        .displayLarge
                        ?.color
                        ?.withOpacity(1)),
              ),
            ),
            Align(alignment: Alignment.centerRight, child: _filterIcon())
          ])
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    margin:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: TextField(
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                      ],
                      controller: TextEditingController(
                          text: store.state.filterValue['Name']),
                      onChanged: (value) {
                        store.dispatch(UpdateFilterValueAction('Name', value));
                      },
                      decoration: InputDecoration(
                        hintText: 'Name',
                        hintStyle: TextStyle(
                          color: Theme.of(context).hintColor.withOpacity(0.54),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context)
                                .dividerColor
                                .withOpacity(0.64),
                          ),
                        ),
                      ),
                    ),
                  )),
              _filterIcon()
            ],
          );
  }

  Widget _filterIcon() {
    return Builder(builder: (context) {
      return InkWell(
        onTap: () => Navigator.of(context).push(PageRouteBuilder(
            barrierColor: Theme.of(context).colorScheme.background,
            pageBuilder: (_, anim1, anim2) => SlideTransition(
                  position: Tween<Offset>(
                          begin: const Offset(1.0, 0.0), end: Offset.zero)
                      .animate(anim1),
                  child: const FilterView(),
                ))),
        child: Icon(
          Icons.filter_alt,
          color: AppColors.leoColor.withOpacity(0.9),
        ),
      );
    });
  }
}
