import 'dart:io' show Platform;

import 'package:employeeviewer/components/employee_list/employee_item.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class FavoriteEmployeeList extends StatelessWidget {
  const FavoriteEmployeeList({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreBuilder<AppState>(
        builder: (context, store) => Container(
            margin: const EdgeInsets.only(top: 12),
            decoration: Platform.isIOS
                ? BoxDecoration(
                    color: Theme.of(context).colorScheme.background,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(12.0),
                        topRight: Radius.circular(12.0)),
                    border: Border.all(
                        color: Theme.of(context).colorScheme.background))
                : null,
            child: ListView.separated(
              scrollDirection: Axis.vertical,
              itemCount: store.state.employeeList
                  .where((element) =>
                      store.state.favoriteEmployeeList.contains(element.id))
                  .length,
              itemBuilder: (context, index) => Employee(
                  employeeData: store.state.employeeList
                      .where((element) =>
                          store.state.favoriteEmployeeList.contains(element.id))
                      .toList()[index]),
              separatorBuilder: (BuildContext context, int index) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Platform.isIOS
                    ? Row(
                        children: [
                          const SizedBox(width: 78.0),
                          Expanded(
                            child: Divider(
                              color:
                                  CupertinoColors.systemGrey.withOpacity(0.3),
                            ),
                          ),
                        ],
                      )
                    : Divider(
                        color: Theme.of(context).dividerColor.withOpacity(0.2)),
              ),
            )));
  }
}
