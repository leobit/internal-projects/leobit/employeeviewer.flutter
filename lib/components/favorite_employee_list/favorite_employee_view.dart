import 'dart:io' show Platform;

import 'package:employeeviewer/components/favorite_employee_list/favorite_employee_list.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:flutter/material.dart';

class FavoriteEmployeeView extends StatelessWidget {
  const FavoriteEmployeeView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
          key: const Key('favorite_list'),
          appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).primaryColor,
              title: Platform.isIOS
                  ? const BackNavigationAppbar(title: 'Favorite List')
                  : const SettingsAppBar(title: 'Favorite List')),
          bottomNavigationBar: const BottomNavigation(currentIndex: 1),
          body: const FavoriteEmployeeList(),
        ));
  }
}
