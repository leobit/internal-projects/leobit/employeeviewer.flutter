import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/notification.dart';
import 'package:employeeviewer/components/notification_list/text_notification_builder.dart';
import 'package:employeeviewer/redux/actions/multi_select_list_action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:employeeviewer/redux/middleware/thunk_notification_action.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:redux/redux.dart';

class NotificationItem extends StatelessWidget {
  const NotificationItem({
    super.key,
    required this.notification,
    required this.typeNotification,
  });

  final NotificationUpdate notification;
  final String typeNotification;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);
    Store<MultiSelectListState> multiSelectListStore =
        StoreProvider.of<MultiSelectListState>(context);

    return Slidable(
      endActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            backgroundColor: Colors.grey,
            foregroundColor: Colors.white,
            icon: Icons.mark_chat_read_outlined,
            label: 'Read',
            onPressed: (BuildContext context) {
              store.dispatch(updateStoredNotification([notification]));
            },
          ),
          SlidableAction(
            onPressed: (BuildContext context) {
              store.dispatch(archiveNotifications([notification]));
            },
            backgroundColor: AppColors.leoColor,
            foregroundColor: Colors.white,
            icon: Icons.archive,
            label: 'Archive',
          ),
        ],
      ),
      child: InkWell(
        onTap: () {
          if (multiSelectListStore.state.isActive) {
            multiSelectListStore.dispatch(AddSelectedItemAction(notification));
          } else {
            store.dispatch(getEmployeeDetails(notification.id));
            Navigator.of(context)
                .pushNamed('/details', arguments: notification.id);
          }
        },
        child: StoreConnector<MultiSelectListState, bool>(
          converter: (store) => store.state.selectedNotificationList
              .map((e) => e.id)
              .contains(notification.id),
          builder: (context, isChecked) => Container(
            padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
            margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            decoration: BoxDecoration(
              color: isChecked
                  ? AppColors.leoColor.withOpacity(0.4)
                  : ((Platform.isIOS)
                      ? Theme.of(context).cardColor
                      : Colors.transparent),
              boxShadow: [
                BoxShadow(
                  color: Platform.isIOS
                      ? Theme.of(context).cardColor
                      : Colors.transparent,
                  spreadRadius: 1,
                  blurRadius: 2,
                ),
              ],
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: StoreConnector<MultiSelectListState, MultiSelectListState>(
              converter: (store) => store.state,
              builder: (context, state) {
                return Stack(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Icon(
                        Icons.mark_chat_read_outlined,
                        color: notification.isChecked
                            ? AppColors.leoColor
                            : Colors.transparent,
                        size: 20,
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        InkWell(
                          onLongPress: () {
                            if (!state.isActive) {
                              multiSelectListStore
                                  .dispatch(InitAction(!state.isActive));
                            }
                          },
                          child: CircleAvatar(
                            radius: 37.5,
                            backgroundImage:
                                const AssetImage('assets/images/leobit.png'),
                            child: notification.photoSmall == null
                                ? null
                                : ClipRRect(
                                    borderRadius: BorderRadius.circular(100.0),
                                    child:
                                        Image.network(notification.photoSmall!),
                                  ),
                          ),
                        ),
                        const SizedBox(width: 8.0),
                        TextNotificationBuilder(
                          typeNotification: typeNotification,
                          notification: notification,
                        ),
                      ],
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
