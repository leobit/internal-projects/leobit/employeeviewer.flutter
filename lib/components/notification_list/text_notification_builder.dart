import 'package:employeeviewer/api/model/notification.dart';
import 'package:flutter/material.dart';

class TextNotificationBuilder extends StatelessWidget {
  const TextNotificationBuilder({
    super.key,
    required this.typeNotification,
    required this.notification,
  });

  final String typeNotification;
  final NotificationUpdate notification;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: RichText(
          text: TextSpan(
            style: Theme.of(context).textTheme.displayLarge,
            children: _buildNotificationText(typeNotification, notification),
          ),
        ),
      ),
    );
  }

  List<TextSpan> _buildNotificationText(
      String typeNotification, NotificationUpdate notification) {
    switch (typeNotification) {
      case 'certificate_update':
        return notification.buildCertificateNotification();

      case 'seniority_update':
        return notification.buildSeniorityNotification();

      case 'leave':
        return notification.buildLeaveNotification();

      case 'newcomer':
      default:
        return notification.buildNewcomerNotification();
    }
  }
}
