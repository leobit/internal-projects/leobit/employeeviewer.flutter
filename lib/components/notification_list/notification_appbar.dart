import 'dart:io' show Platform;

import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/actions/multi_select_list_action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_notification_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class NotificationAppbar extends StatelessWidget {
  const NotificationAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<MultiSelectListState, MultiSelectListState>(
        converter: (store) => store.state,
        builder: (context, state) => (Platform.isIOS)
            ? (state.isActive
                ? BackNavigationAppbar(
                    leading: _closeMultiselectOption(context, state),
                    trailing: _multiselectAction(context, state),
                  )
                : BackNavigationAppbar(
                    title: 'Notification',
                    trailing: _archiveSectionButton(context)))
            : (state.isActive
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                        Expanded(
                          flex: 3,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: _closeMultiselectOption(context, state),
                          ),
                        ),
                        Expanded(
                            flex: 2, child: _multiselectAction(context, state))
                      ])
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SettingsAppBar(title: 'Notification'),
                      _archiveSectionButton(context)
                    ],
                  )));
  }

  Widget _closeMultiselectOption(
      BuildContext context, MultiSelectListState state) {
    Store<MultiSelectListState> multiSelectStore = StoreProvider.of(context);

    return InkWell(
        onTap: () => multiSelectStore.dispatch(InitAction(!state.isActive)),
        child: StoreConnector<MultiSelectListState, int>(
            converter: (store) => store.state.selectedNotificationList.length,
            builder: (context, count) {
              return SettingsAppBar(title: 'Close ($count)');
            }));
  }

  Widget _multiselectAction(BuildContext context, MultiSelectListState state) {
    Store<AppState> store = StoreProvider.of(context);

    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      InkWell(
          onTap: () {
            store.dispatch(
                updateStoredNotification(state.selectedNotificationList));
            StoreProvider.of<MultiSelectListState>(context)
                .dispatch(InitAction(!state.isActive));
          },
          child: const Text(
            'Read',
            style: TextStyle(fontSize: 20, color: Colors.grey),
          )),
      InkWell(
          onTap: () {
            store
                .dispatch(archiveNotifications(state.selectedNotificationList));
            StoreProvider.of<MultiSelectListState>(context)
                .dispatch(InitAction(!state.isActive));
          },
          child: const Text(
            'Archive',
            style: TextStyle(fontSize: 20, color: Colors.grey),
          ))
    ]);
  }

  Widget _archiveSectionButton(BuildContext context) {
    return InkWell(
        onTap: () => Navigator.of(context).pushNamed('/archived-notification'),
        child: const Text(
          'Archive',
          style: TextStyle(fontSize: 20, color: Colors.grey),
        ));
  }
}
