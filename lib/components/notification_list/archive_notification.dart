import 'dart:io' show Platform;

import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'archive_item.dart';

class ArchiveNotification extends StatelessWidget {
  const ArchiveNotification({super.key});

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of(context);

    return SafeArea(
        bottom: false,
        top: false,
        child: StoreConnector<AppState, int>(
            converter: (store) => store.state.differencesEmployee.length,
            builder: (context, count) {
              var archivedNotifications = store.state.archivedNotifications;
              return Scaffold(
                bottomNavigationBar: const BottomNavigation(currentIndex: 2),
                appBar: AppBar(
                    automaticallyImplyLeading: false,
                    backgroundColor: Theme.of(context).primaryColor,
                    title: (Platform.isIOS)
                        ? const BackNavigationAppbar(
                            title: 'Archive Notification')
                        : const SettingsAppBar(title: 'Archive notification')),
                body: (count == 0)
                    ? Center(
                        child: Text(
                          'No archived notifications :)',
                          style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.w800,
                              color: Theme.of(context)
                                  .textTheme
                                  .displayMedium
                                  ?.color),
                        ),
                      )
                    : SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Container(
                          margin: const EdgeInsets.symmetric(vertical: 12),
                          child: Column(
                            children: archivedNotifications
                                .map((notification) => ArchiveItem(
                                    notification: notification,
                                    typeNotification:
                                        notification.notificationType!))
                                .toList(),
                          ),
                        ),
                      ),
              );
            }));
  }
}
