import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/notification.dart';
import 'package:employeeviewer/components/notification_list/text_notification_builder.dart';
import 'package:flutter/material.dart';

class ArchiveItem extends StatelessWidget {
  const ArchiveItem({
    super.key,
    required this.notification,
    required this.typeNotification,
  });

  final NotificationUpdate notification;
  final String typeNotification;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
      margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      decoration: BoxDecoration(
        color: ((Platform.isIOS)
            ? Theme.of(context).cardColor
            : Colors.transparent),
        boxShadow: [
          BoxShadow(
            color: Platform.isIOS
                ? Theme.of(context).cardColor
                : Colors.transparent,
            spreadRadius: 1,
            blurRadius: 2,
          ),
        ],
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          InkWell(
            child: CircleAvatar(
              radius: 37.5,
              backgroundImage: const AssetImage('assets/images/leobit.png'),
              child: notification.photoSmall == null
                  ? null
                  : ClipRRect(
                      borderRadius: BorderRadius.circular(100.0),
                      child: Image.network(notification.photoSmall!),
                    ),
            ),
          ),
          TextNotificationBuilder(
            notification: notification,
            typeNotification: typeNotification,
          ),
        ],
      ),
    );
  }
}
