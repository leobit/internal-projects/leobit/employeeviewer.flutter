import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/notification_list/notification_appbar.dart';
import 'package:employeeviewer/components/notification_list/notification_item.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/reducer/multi_select_list_reducer.dart';
import 'package:employeeviewer/redux/selectors/notification_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class NotificationList extends StatelessWidget {
  const NotificationList({super.key});

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of(context);

    return StoreProvider(
      store: Store<MultiSelectListState>(multiselectReducer,
          initialState: MultiSelectListState.init),
      child: SafeArea(
          bottom: false,
          top: false,
          child: StoreConnector<AppState, int>(
              converter: (store) =>
                  notificationCountSelector(store.state.differencesEmployee),
              builder: (context, count) {
                var notifications = store.state.differencesEmployee;
                var savedNotifications = store.state.savedNotifications;

                return Scaffold(
                  bottomNavigationBar: const BottomNavigation(currentIndex: 2),
                  appBar: AppBar(
                      automaticallyImplyLeading: false,
                      backgroundColor: Theme.of(context).primaryColor,
                      title: const NotificationAppbar()),
                  body: (count == 0 && savedNotifications.isEmpty)
                      ? Center(
                          child: Text(
                            'No new notifications :)',
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w800,
                                color: Theme.of(context)
                                    .textTheme
                                    .displayMedium
                                    ?.color),
                          ),
                        )
                      : SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            margin: const EdgeInsets.symmetric(vertical: 12),
                            child: Column(
                              children: [
                                Column(
                                    children: notifications.keys
                                        .toList()
                                        .map((indexKey) => Column(
                                              children: notifications[indexKey]!
                                                  .map((notification) =>
                                                      NotificationItem(
                                                          notification:
                                                              notification,
                                                          typeNotification:
                                                              indexKey))
                                                  .toList(),
                                            ))
                                        .toList()),
                                Column(
                                  children: savedNotifications
                                      .map((notification) => NotificationItem(
                                          notification: notification,
                                          typeNotification:
                                              notification.notificationType!))
                                      .toList(),
                                )
                              ],
                            ),
                          ),
                        ),
                );
              })),
    );
  }
}
