import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/components/employee_details/employee_general_info_section.dart';
import 'package:employeeviewer/components/employee_details/employee_professional_skills_section.dart';
import 'package:employeeviewer/components/employee_details/logged_time.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'employee_photo_box.dart';

class EmployeeDetails extends StatelessWidget {
  const EmployeeDetails({super.key});

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          EmployeeData? employee = state.employeeData;

          if (employee != null) {
            return _employeeDescription(employee, store, context);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  Widget _employeeDescription(
      EmployeeData employee, Store store, BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 6),
          child: Column(
            children: [
              EmployeePhotoBox(
                name: employee.fullName,
                photo: employee.photoMedium,
                profile: employee.profile ?? '',
                position: employee.position ?? '',
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.96,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LoggedTime(time: employee.intHours),
                    const SizedBox(height: 14.0),
                    EmployeeGeneralInfoSection(employee: employee),
                    EmployeeProfessionalSkillsSection(employee: employee),
                    (store.state.employeeData?.id ==
                                store.state.personalDetails?.id &&
                            Platform.isIOS)
                        ? Center(
                            child: Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 20),
                                child: const Text(
                                  'Logout',
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 24),
                                )))
                        : Container()
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
