import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';

class LoggedTime extends StatelessWidget {
  const LoggedTime({super.key, required this.time});

  final String time;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            time,
            style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w800,
                color: AppColors.leoColor.withOpacity(0.8)),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'hours',
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: AppColors.leoColor.withOpacity(0.8)),
            ),
            Text(
              'logged',
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: AppColors.leoColor.withOpacity(0.8)),
            ),
          ],
        )
      ],
    );
  }
}
