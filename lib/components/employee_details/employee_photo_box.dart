import 'package:flutter/material.dart';

class EmployeePhotoBox extends StatelessWidget {
  const EmployeePhotoBox({
    super.key,
    required this.name,
    required this.photo,
    required this.profile,
    required this.position,
  });

  final String name;
  final String? photo;
  final String profile;
  final String position;

  @override
  Widget build(BuildContext context) {
    final textColor =
        Theme.of(context).textTheme.bodyMedium!.color!.withOpacity(0.76);

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 12),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 30),
            child: photo != null
                ? CircleAvatar(
                    radius: 128.0,
                    backgroundImage:
                        const AssetImage('assets/images/leobit.png'),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(128.0),
                      child: Image.network(photo!),
                    ))
                : const CircleAvatar(
                    radius: 128.0,
                    backgroundImage: AssetImage('assets/images/leobit.png'),
                  ),
          ),
          Text(name,
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: 22,
                color: textColor,
              )),
          FittedBox(
              child: Text(
            '$profile $position',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .merge(TextStyle(color: textColor)),
          )),
        ],
      ),
    );
  }
}
