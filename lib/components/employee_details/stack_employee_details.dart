import 'package:flutter/material.dart';

class StackEmployeeDetails extends StatelessWidget {
  const StackEmployeeDetails({
    super.key,
    this.left,
    this.right,
  });

  final Widget? left;
  final Widget? right;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Align(alignment: Alignment.centerLeft, child: left),
          ),
          Expanded(
            child: Align(alignment: Alignment.centerLeft, child: right),
          ),
          // ),
        ],
      ),
    );
  }
}
