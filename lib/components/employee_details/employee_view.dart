import 'dart:io' show Platform;

import 'package:employeeviewer/components/employee_details/employee_details.dart';
import 'package:employeeviewer/components/employee_details/slack_button.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:employeeviewer/redux/selectors/employee_list_selector.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class EmployeeView extends StatelessWidget {
  const EmployeeView({super.key});

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments;
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
            bottomNavigationBar: BottomNavigation(
                currentIndex:
                    (args == store.state.personalDetails?.id) ? 4 : 0),
            appBar: (Platform.isIOS
                ? CupertinoNavigationBar(
                    backgroundColor: Theme.of(context).primaryColor,
                    leading: GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                      child: const Row(
                        children: [
                          Icon(Icons.arrow_back_ios,
                              color: CupertinoColors.systemGrey),
                          Text(
                            'Back',
                            style: TextStyle(
                                fontSize: 20,
                                color: CupertinoColors.systemGrey),
                          ),
                        ],
                      ),
                    ),
                    middle: Text(
                      'Details',
                      style: TextStyle(
                          fontSize: Theme.of(context)
                              .textTheme
                              .displayLarge
                              ?.fontSize,
                          color: Theme.of(context)
                              .textTheme
                              .displayLarge
                              ?.color
                              ?.withOpacity(1)),
                    ),
                    trailing: _favoriteMark(store, args.toString()),
                  )
                : AppBar(
                    automaticallyImplyLeading: false,
                    scrolledUnderElevation: 0.0,
                    backgroundColor: Theme.of(context).primaryColor,
                    toolbarHeight: 40,
                    title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SettingsAppBar(title: 'Employee Details'),
                          _favoriteMark(store, args.toString()),
                        ]),
                  )) as PreferredSizeWidget,
            floatingActionButton: const SlackButton(),
            body: const EmployeeDetails()));
  }

  Widget _favoriteMark(Store<AppState> store, String employeeId) {
    return GestureDetector(
        onTap: () {
          store.dispatch(toggleFavoriteEmployee(employeeId));
        },
        child: StoreConnector<AppState, bool>(
          converter: (store) =>
              favoriteEmployeeSelector(store.state, employeeId),
          builder: (context, isFavorite) => Icon(
            (isFavorite ? Icons.favorite : Icons.favorite_border),
            size: 25,
            color: AppColors.leoColor,
          ),
        ));
  }
}
