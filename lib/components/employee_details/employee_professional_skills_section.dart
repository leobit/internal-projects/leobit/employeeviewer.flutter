import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/components/employee_details/employee_info_block.dart';
import 'package:employeeviewer/components/employee_details/employee_section_header.dart';
import 'package:employeeviewer/components/employee_details/stack_employee_details.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class EmployeeProfessionalSkillsSection extends StatelessWidget {
  const EmployeeProfessionalSkillsSection({super.key, required this.employee});

  final EmployeeData employee;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20.0),
        Column(
          children: [
            const EmployeeSectionHeader('Professional skills'),
            Platform.isIOS
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      EmployeeInfoBlock(
                        title: 'Specialization',
                        value: employee.profile ?? '-',
                        isSeparated: true,
                      ),
                      EmployeeInfoBlock(
                        title: 'Seniority',
                        value: employee.seniority ?? '-',
                        isSeparated: true,
                      ),
                      EmployeeInfoBlock(
                        title: 'Seniority sub-level',
                        value: employee.senioritySublevel ?? '-',
                        isSeparated: true,
                      ),
                      EmployeeInfoBlock(
                        title: 'English level',
                        value: employee.englishLevel ?? '-',
                        isSeparated: true,
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 6.0),
                      StackEmployeeDetails(
                        left: EmployeeInfoBlock(
                          title: 'Seniority',
                          value: employee.seniority ?? '-',
                          isSeparated: true,
                        ),
                        right: EmployeeInfoBlock(
                          title: 'Specialization',
                          value: employee.profile ?? '-',
                          isSeparated: true,
                        ),
                      ),
                      StackEmployeeDetails(
                        left: EmployeeInfoBlock(
                          title: 'Seniority sub-level',
                          value: employee.senioritySublevel ?? '-',
                          isSeparated: true,
                        ),
                        right: EmployeeInfoBlock(
                          title: 'English level',
                          value: employee.englishLevel ?? '-',
                        ),
                      )
                    ],
                  ),
          ],
        ),
        Platform.isIOS
            ? _iOSCertificatesAndProject(context, store, employee)
            : _androidCertificatesAndProject(context, store, employee),
        const SizedBox(height: 20.0),
      ],
    );
  }

  Widget _iOSCertificatesAndProject(
      BuildContext context, Store<AppState> store, EmployeeData employee) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (employee.certificates != null && employee.certificates!.isNotEmpty)
          Container(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
            decoration: const BoxDecoration(
              color: Colors.transparent,
              border: Border(
                bottom:
                    BorderSide(color: CupertinoColors.systemGrey, width: 0.75),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Certificates:',
                  style: TextStyle(fontSize: 19.0),
                ),
                const SizedBox(width: 16.0),
                Flexible(
                  child: InkWell(
                    onTap: () =>
                        Navigator.of(context).pushNamed('/certificates'),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: employee.certificates!
                            .split('\n')
                            .map(
                              (certificate) => Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 2.0),
                                child: Text(
                                  certificate,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: _getValueTextStyle(
                                      CupertinoColors.systemGrey,
                                      FontWeight.w500,
                                      height: 1.22),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Projects:',
                style: TextStyle(fontSize: 19),
              ),
              const SizedBox(width: 16.0),
              Flexible(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: employee.projects!
                        .map((project) => InkWell(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 2.0),
                                child: Text(
                                  project,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: _getValueTextStyle(
                                      CupertinoColors.systemGrey,
                                      FontWeight.w500,
                                      height: 1.22),
                                ),
                              ),
                              onTap: () => store.dispatch(
                                filterAndNavigateToEmployeeList(
                                  context,
                                  employee,
                                  'Project',
                                  [project],
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _androidCertificatesAndProject(
      BuildContext context, Store<AppState> store, EmployeeData employee) {
    final androidTitleStyle = Theme.of(context).textTheme.headlineSmall!.merge(
          const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
        );
    final androidValueColor =
        Theme.of(context).textTheme.displaySmall!.color!.withOpacity(0.76);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (employee.certificates != null && employee.certificates!.isNotEmpty)
          Container(
            padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Certificates', style: androidTitleStyle),
                const SizedBox(height: 1.0),
                InkWell(
                  onTap: () => Navigator.of(context).pushNamed('/certificates'),
                  child: ListView(
                    shrinkWrap: true,
                    children: employee.certificates!
                        .split('\n')
                        .map(
                          (certificate) => Text(
                            certificate,
                            maxLines: 3,
                            style: _getValueTextStyle(
                                androidValueColor, FontWeight.w800),
                          ),
                        )
                        .toList(),
                  ),
                ),
              ],
            ),
          ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Projects', style: androidTitleStyle),
              const SizedBox(height: 1.0),
              ListView.builder(
                shrinkWrap: true,
                itemCount: employee.projects!.length,
                itemBuilder: (context, index) {
                  final project = employee.projects![index];

                  return InkWell(
                    onTap: () => store.dispatch(filterAndNavigateToEmployeeList(
                      context,
                      employee,
                      'Project',
                      [project],
                    )),
                    child: Text(
                      project,
                      maxLines: 3,
                      style: _getValueTextStyle(
                          androidValueColor, FontWeight.w800),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  TextStyle _getValueTextStyle(Color color, FontWeight fontWeight,
      {double? height}) {
    return TextStyle(
      fontSize: 18.5,
      color: Colors.transparent,
      fontWeight: fontWeight,
      height: height,
      shadows: [
        Shadow(
          color: color,
          offset: const Offset(0, -1.0),
        )
      ],
      decoration: TextDecoration.underline,
      decorationThickness: 1.0,
      decorationColor: color,
    );
  }
}
