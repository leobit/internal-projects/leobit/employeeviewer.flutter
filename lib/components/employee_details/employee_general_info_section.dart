import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/components/employee_details/employee_info_block.dart';
import 'package:employeeviewer/components/employee_details/employee_section_header.dart';
import 'package:employeeviewer/components/employee_details/stack_employee_details.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:url_launcher/url_launcher_string.dart';

class EmployeeGeneralInfoSection extends StatelessWidget {
  const EmployeeGeneralInfoSection({super.key, required this.employee});

  final EmployeeData employee;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return Column(
      children: [
        const EmployeeSectionHeader('General information'),
        Platform.isIOS
            ? _iOSSection(store, context)
            : _androidSection(store, context),
      ],
    );
  }

  Widget _iOSSection(Store store, BuildContext context) {
    return Column(
      children: [
        EmployeeInfoBlock(
          title: 'Employment',
          value: employee.employment,
          isSeparated: true,
        ),
        EmployeeInfoBlock(
          title: 'Start date',
          value: employee.startDate.substring(0, 10),
          isSeparated: true,
        ),
        EmployeeInfoBlock(
          title: 'Mentor',
          value: employee.mentor ?? '-',
          isSeparated: true,
        ),
        EmployeeInfoBlock(
          title: 'Manager',
          value: employee.manager ?? '-',
          isSeparated: true,
          onTap: () => store.dispatch(filterAndNavigateToEmployeeList(
            context,
            employee,
            'Name',
            employee.manager,
          )),
        ),
        InkWell(
          onTap: () => _launchEmail(employee.email),
          child: EmployeeInfoBlock(
            title: 'E-mail',
            value: employee.email,
            isSeparated: true,
          ),
        ),
        EmployeeInfoBlock(
          title: 'Phone',
          value: _customizedPhoneNumber(employee.phone),
        ),
      ],
    );
  }

  Widget _androidSection(Store store, BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 6.0),
        StackEmployeeDetails(
          left: EmployeeInfoBlock(
            title: 'Employment:',
            value: employee.employment,
          ),
          right: EmployeeInfoBlock(
            title: 'Start date:',
            value: employee.startDate.substring(0, 10),
          ),
        ),
        StackEmployeeDetails(
          left: EmployeeInfoBlock(
            title: 'Mentor:',
            value: employee.mentor ?? '-',
          ),
          right: EmployeeInfoBlock(
            title: 'Manager:',
            value: employee.manager ?? '-',
            onTap: () => store.dispatch(filterAndNavigateToEmployeeList(
              context,
              employee,
              'Name',
              employee.manager,
            )),
          ),
        ),
        InkWell(
          onTap: () => _launchEmail(employee.email),
          child: EmployeeInfoBlock(
            title: 'E-mail:',
            value: employee.email,
          ),
        ),
        EmployeeInfoBlock(
          title: 'Phone:',
          value: _customizedPhoneNumber(employee.phone),
        ),
      ],
    );
  }

  void _launchEmail(String email) async {
    await launchUrlString('mailto:$email');
  }

  String _customizedPhoneNumber(String? phone) {
    return (phone == null)
        ? '-'
        : '+${phone.substring(0, 2)} '
            '(${phone.substring(2, 5)}) '
            '${phone.substring(5, 8)}-'
            '${phone.substring(8, 10)}-'
            '${phone.substring(10, 12)}';
  }
}
