import 'package:employeeviewer/template/certificates.dart';
import 'package:flutter/material.dart';

class CertificateItem extends StatelessWidget {
  const CertificateItem({super.key, required this.certificate});

  final String certificate;

  @override
  Widget build(BuildContext context) {
    final certificateImage = certificateImages[certificate];

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 6),
      padding: const EdgeInsets.symmetric(horizontal: 12),
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade300.withOpacity(0.7),
            spreadRadius: 3,
            blurRadius: 2,
            offset: const Offset(0, 1),
          ),
        ],
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: CircleAvatar(
              radius: 55,
              backgroundColor: Colors.transparent,
              child: certificateImage == null
                  ? const SizedBox()
                  : certificateImage.local
                      ? Image.asset(certificateImage.path)
                      : Image.network(certificateImage.path),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              certificate,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 22,
                  color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
