import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmployeeInfoBlock extends StatelessWidget {
  const EmployeeInfoBlock({
    super.key,
    required this.title,
    this.value,
    this.isSeparated,
    this.onTap,
  });

  final String title;
  final String? value;
  final bool? isSeparated;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS ? _iOSInfoBlock() : _androidInfoBlock(context);
  }

  Widget _iOSInfoBlock() {
    return Row(
      children: [
        Expanded(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            margin: const EdgeInsets.symmetric(horizontal: 10.0),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: isSeparated == true
                  ? const Border(
                      bottom: BorderSide(
                          color: CupertinoColors.systemGrey, width: 0.75),
                    )
                  : null,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(title, style: const TextStyle(fontSize: 19.0)),
                const SizedBox(width: 10.0),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: onTap,
                      child: Text(
                        value as String,
                        maxLines: 1,
                        style: _getValueTextStyle(
                          CupertinoColors.systemGrey,
                          FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _androidInfoBlock(BuildContext context) {
    final titleTextStyle = Theme.of(context).textTheme.headlineSmall!;
    final valueColor =
        Theme.of(context).textTheme.displaySmall!.color!.withOpacity(0.76);

    return Container(
      margin: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: titleTextStyle.merge(
              const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
            ),
          ),
          const SizedBox(height: 1.0),
          InkWell(
            onTap: onTap,
            child: Text(
              value as String,
              maxLines: 3,
              style: _getValueTextStyle(
                valueColor,
                FontWeight.w800,
              ),
            ),
          ),
        ],
      ),
    );
  }

  TextStyle _getValueTextStyle(
    Color color,
    FontWeight fontWeight,
  ) {
    var valueStyle = TextStyle(
      overflow: TextOverflow.ellipsis,
      fontSize: 18.5,
      color: color,
      fontWeight: fontWeight,
    );

    if (onTap != null) {
      valueStyle = valueStyle.merge(TextStyle(
        color: Colors.transparent,
        shadows: [
          Shadow(
            color: color,
            offset: const Offset(0, -1.5),
          ),
        ],
        decoration: TextDecoration.underline,
        decorationThickness: 1.0,
        decorationColor: color,
      ));
    }

    return valueStyle;
  }
}
