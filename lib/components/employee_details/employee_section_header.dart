import 'package:flutter/material.dart';

class EmployeeSectionHeader extends StatelessWidget {
  const EmployeeSectionHeader(this.title, {super.key});

  final String title;

  @override
  Widget build(BuildContext context) {
    final headerColor = Theme.of(context).dividerColor.withOpacity(0.28);

    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: headerColor,
              fontSize: 15.5,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 6.0),
          Divider(height: 1.0, color: headerColor.withOpacity(0.26)),
        ],
      ),
    );
  }
}
