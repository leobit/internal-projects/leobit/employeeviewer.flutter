import 'package:employeeviewer/components/custom_icons/slack_icon.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:url_launcher/url_launcher.dart';

class SlackButton extends StatelessWidget {
  const SlackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) => (state.employeeData?.slackUserId == null)
          ? Container()
          : FloatingActionButton(
              backgroundColor: AppColors.leoColor,
              heroTag: 'goSlackDirect',
              onPressed: () => _launchSlackDirect(
                  state.employeeData!.slackUserId!, state.teamId),
              child: const Icon(SlackIcon.slack),
            ),
    );
  }

  void _launchSlackDirect(String employeeSlackId, String teamId) async {
    await launch('slack://user?team=$teamId=$employeeSlackId');
  }
}
