import 'dart:io';

import 'package:employeeviewer/components/employee_details/certificate_item.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class EmployeeCertificatesView extends StatelessWidget {
  const EmployeeCertificatesView({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
      converter: (store) => store.state,
      builder: (context, state) {
        final displayLargeStyle = Theme.of(context).textTheme.displayLarge;

        return Scaffold(
          appBar: (Platform.isIOS
              ? CupertinoNavigationBar(
                  backgroundColor: Theme.of(context).primaryColor,
                  leading: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: const Row(
                      children: [
                        Icon(Icons.arrow_back_ios,
                            color: CupertinoColors.systemGrey),
                        Text(
                          'Back',
                          style: TextStyle(
                              fontSize: 20, color: CupertinoColors.systemGrey),
                        ),
                      ],
                    ),
                  ),
                  middle: Text(
                    'Certificates',
                    style: TextStyle(
                      fontSize: displayLargeStyle?.fontSize,
                      color: displayLargeStyle?.color?.withOpacity(1),
                    ),
                  ),
                )
              : AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Theme.of(context).primaryColor,
                  toolbarHeight: 40,
                  title: const SettingsAppBar(title: 'Certificates'),
                )) as PreferredSizeWidget,
          body: state.employeeData?.certificates == null
              ? Center(
                  child: Text(
                    'No certificates :)',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w800,
                        color:
                            Theme.of(context).textTheme.displayMedium?.color),
                  ),
                )
              : Center(
                  child: SingleChildScrollView(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: state.employeeData!.certificates!
                        .split('\n')
                        .map((certificate) =>
                            CertificateItem(certificate: certificate))
                        .toList(),
                  )),
                ),
        );
      },
    );
  }
}
