import 'dart:async';

import 'package:employeeviewer/api/auth/auth_api.dart';
import 'package:employeeviewer/components/login/login_appbar.dart';
import 'package:employeeviewer/components/login/login_box.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    Future(() async {
      final AuthApi auth = AuthApi();
      if (await auth.getExpTime() != null) {
        await Navigator.of(context).popAndPushNamed('/list');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);
    store.dispatch(updateThemeState());
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).primaryColor,
              title: const CustomAppBar()),
          body: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [LoginContainer()],
              ),
            ],
          )),
    );
  }
}
