import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      key: const Key('login_app_bar'),
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.network(
          'https://employee.leobit.co/assets/scalableVectorGraphics/logo.svg',
          placeholderBuilder: (context) => Container(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ],
    );
  }
}
