import 'dart:io' show Platform;

import 'package:employeeviewer/components/corporate_rules/expension_panel_element.dart';
import 'package:employeeviewer/template/corporate_rules.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CorporateRulesSection extends StatelessWidget {
  const CorporateRulesSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? SingleChildScrollView(
            child: Column(
              children: corporateRulesTitleList
                  .map((corporateRules) =>
                      CustomExpansionPanel(data: corporateRules))
                  .toList(),
            ),
          )
        : ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: corporateRulesTitleList.length,
            itemBuilder: (context, index) => CustomExpansionPanel(
              data: corporateRulesTitleList[index],
            ),
          );
  }
}
