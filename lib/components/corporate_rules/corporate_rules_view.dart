import 'dart:io' show Platform;

import 'package:employeeviewer/components/corporate_rules/corporate_rules_section.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:flutter/material.dart';

class CorporateRulesView extends StatelessWidget {
  const CorporateRulesView({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).primaryColor,
              title: Platform.isIOS
                  ? const BackNavigationAppbar(title: 'Corporate Rules')
                  : const SettingsAppBar(title: 'Corporate Rules')),
          bottomNavigationBar: const BottomNavigation(currentIndex: 4),
          body: const CorporateRulesSection()),
    );
  }
}
