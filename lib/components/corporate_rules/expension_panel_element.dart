import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomExpansionPanel extends StatelessWidget {
  const CustomExpansionPanel({super.key, required this.data});

  final CorporateRulesTitle? data;

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? _iosCorporateRulesItem(context)
        : _androidCorporateRulesItem(context);
  }

  Widget _iosCorporateRulesItem(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      margin: const EdgeInsets.all(12),
      child: Column(
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              child: Text(
                data?.topic as String,
                style: const TextStyle(
                    fontSize: 14, color: CupertinoColors.systemGrey),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.background,
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                border: Border.all(
                    color: Theme.of(context).colorScheme.background)),
            child: ListView.separated(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) => Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 12),
                      child: InkWell(
                        onTap: () =>
                            _launchURL(data?.urlList[index].url as String),
                        child: Text(
                          data?.urlList[index].title as String,
                          style: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .color,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .fontSize,
                              fontWeight: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .fontWeight),
                        ),
                      ),
                    ),
                separatorBuilder: (context, index) =>
                    const Divider(color: CupertinoColors.systemGrey),
                itemCount: data!.urlList.length),
          )
        ],
      ),
    ));
  }

  Widget _androidCorporateRulesItem(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 12),
      child: SingleChildScrollView(
        child: Column(
          children: [
            ListTile(
              leading: Text(data?.topic as String,
                  style: TextStyle(
                      fontSize:
                          Theme.of(context).textTheme.displayLarge?.fontSize,
                      color: Theme.of(context).textTheme.displaySmall?.color)),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) => ListTile(
                        onTap: () =>
                            _launchURL(data?.urlList[index].url as String),
                        leading: Text(data?.urlList[index].title as String,
                            style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .displaySmall!
                                    .color,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .fontSize,
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .headlineMedium!
                                    .fontWeight)),
                      ),
                  separatorBuilder: (context, index) =>
                      const Divider(color: CupertinoColors.systemGrey),
                  itemCount: data!.urlList.length),
            )
          ],
        ),
      ),
    );
  }
}

void _launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
