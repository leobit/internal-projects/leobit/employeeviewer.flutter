import 'dart:io' show Platform;

import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/components/statistic/employee_statistic.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StatisticView extends StatelessWidget {
  const StatisticView({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 7,
        child: SafeArea(
            bottom: false,
            top: false,
            child: Scaffold(
              appBar: ((Platform.isIOS)
                  ? CupertinoNavigationBar(
                      backgroundColor: Theme.of(context).primaryColor,
                      leading: InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: const Row(
                          children: [
                            Icon(Icons.arrow_back_ios,
                                color: CupertinoColors.systemGrey),
                            Text(
                              'Back',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: CupertinoColors.systemGrey),
                            ),
                          ],
                        ),
                      ),
                      middle: Text(
                        'Statistics',
                        style: TextStyle(
                            fontSize: Theme.of(context)
                                .textTheme
                                .displayLarge
                                ?.fontSize,
                            color: Theme.of(context)
                                .textTheme
                                .displayLarge
                                ?.color
                                ?.withOpacity(1)),
                      ))
                  : AppBar(
                      automaticallyImplyLeading: false,
                      backgroundColor: Theme.of(context).primaryColor,
                      title: const SettingsAppBar(
                        key: Key('statistic_appBar'),
                        title: 'Statistics',
                      ),
                      bottom: TabBar(
                        isScrollable: true,
                        indicatorColor: AppColors.leoColor,
                        labelStyle: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                        tabs: const [
                          Tab(text: 'Hired Year'),
                          Tab(text: 'Total Experience'),
                          Tab(text: 'Managers'),
                          Tab(text: 'Profile'),
                          Tab(text: 'Seniority'),
                          Tab(text: 'Certificates'),
                          Tab(text: 'Top Projects'),
                        ],
                      ))) as PreferredSizeWidget,
              bottomNavigationBar: const BottomNavigation(currentIndex: 3),
              body: const EmployeeStatistic(),
            )));
  }
}
