import 'package:employeeviewer/api/model/statistic_model.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class EmployeeBarStatistic extends StatelessWidget {
  const EmployeeBarStatistic({
    super.key,
    required this.dataSource,
    required this.title,
  });

  final List<BarStatistic> dataSource;
  final String title;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final ChartAxis primaryXAxis;
        final ZoomPanBehavior? zoomPanBehavior;
        final maximumLabelWidth = constraints.maxWidth / 2;
        if (dataSource.length > 20) {
          primaryXAxis = CategoryAxis(
            maximumLabels: 10,
            autoScrollingDelta: 20,
            maximumLabelWidth: maximumLabelWidth,
          );

          zoomPanBehavior = ZoomPanBehavior(
            enablePanning: true,
          );
        } else {
          primaryXAxis = CategoryAxis(
            maximumLabelWidth: maximumLabelWidth,
          );

          zoomPanBehavior = null;
        }

        return SfCartesianChart(
            primaryXAxis: primaryXAxis,
            title: ChartTitle(
              text: title,
              textStyle: const TextStyle(fontSize: 20),
            ),
            primaryYAxis:
                NumericAxis(edgeLabelPlacement: EdgeLabelPlacement.shift),
            zoomPanBehavior: zoomPanBehavior,
            series: <CartesianSeries>[
              BarSeries<BarStatistic, String>(
                  dataSource: dataSource,
                  xValueMapper: (BarStatistic data, _) => data.key,
                  yValueMapper: (BarStatistic data, _) => data.count,
                  color: AppColors.leoColor)
            ]);
      },
    );
  }
}
