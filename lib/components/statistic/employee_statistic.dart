import 'dart:io';

import 'package:employeeviewer/api/model/statistic_model.dart';
import 'package:employeeviewer/components/statistic/employee_bar_statistic.dart';
import 'package:employeeviewer/components/statistic/employee_histogram_statistic.dart';
import 'package:employeeviewer/components/statistic/statistic_item.dart';
import 'package:employeeviewer/redux/actions/tab_navigation_action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/selectors/employee_list_selector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class EmployeeStatistic extends StatelessWidget {
  const EmployeeStatistic({super.key});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, StatisticModel>(
      converter: (store) => employeeStatisticSelector(store.state),
      builder: (context, statistic) => (statistic.yearHiredList.isEmpty)
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Platform.isIOS
              ? StoreBuilder<TabNavigationState>(
                  builder: (context, store) => Column(
                        children: [
                          Expanded(
                            flex: 1,
                            child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  margin: const EdgeInsets.all(12),
                                  child: CupertinoSlidingSegmentedControl(
                                    groupValue: store.state.statisticTabState,
                                    onValueChanged: (value) => store.dispatch(
                                        UpdateStatisticTabState(
                                            value.toString())),
                                    children: {
                                      'hiredYear': Text(
                                        'Hired Year',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'totalExperience': Text(
                                        'Total Experience',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'managers': Text(
                                        'Managers',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'profile': Text(
                                        'Profile',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'seniority': Text(
                                        'Seniority',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'certificates': Text(
                                        'Certificates',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      ),
                                      'topProjects': Text(
                                        'Top Projects',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    },
                                  ),
                                );
                              },
                            ),
                          ),
                          Expanded(
                              flex: 10,
                              child: _renderCupertinoSlidingStatistic(context,
                                  store.state.statisticTabState, statistic))
                        ],
                      ))
              : TabBarView(
                  children: _statisticViewList(context, statistic),
                ),
    );
  }

  Widget _renderCupertinoSlidingStatistic(
      BuildContext context, String tabValue, StatisticModel statistic) {
    List<Widget> statisticList = _statisticViewList(context, statistic);

    if (tabValue == 'hiredYear') {
      return statisticList[0];
    } else if (tabValue == 'totalExperience') {
      return statisticList[1];
    } else if (tabValue == 'managers') {
      return statisticList[2];
    } else if (tabValue == 'profile') {
      return statisticList[3];
    } else if (tabValue == 'seniority') {
      return statisticList[4];
    } else if (tabValue == 'certificates') {
      return statisticList[5];
    } else if (tabValue == 'topProjects') {
      return statisticList[6];
    } else {
      return statisticList[0];
    }
  }

  List<Widget> _statisticViewList(
      BuildContext context, StatisticModel statistic) {
    return [
      EmployeeHistogramStatistic(
          title: 'Statistics of hired year Leobit',
          dataSource: statistic.yearHiredList),
      EmployeeHistogramStatistic(
          title: 'Employees total experience at Leobit',
          dataSource: statistic.totalExperienceList),
      EmployeeBarStatistic(
          title: 'Managers with most subordinates',
          dataSource: statistic.projectManagerList),
      EmployeeBarStatistic(
          title: 'Statistics of profile on Leobit',
          dataSource: statistic.profileList),
      EmployeeBarStatistic(
          title: 'Statistics of seniority on Leobit',
          dataSource: statistic.seniorityList),
      SingleChildScrollView(
        child: Column(
          children: [
            Container(
                margin: const EdgeInsets.all(12),
                child: const Text(
                  'Certification statistics on Leobit',
                  style: TextStyle(fontSize: 20),
                )),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.65,
              child: ListView.separated(
                  itemBuilder: (context, index) =>
                      StatisticItem(data: statistic.certificateList[index]),
                  separatorBuilder: (context, index) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: Divider(
                          color:
                              Platform.isIOS ? Colors.transparent : Colors.grey,
                        ),
                      ),
                  itemCount: statistic.certificateList.length),
            )
          ],
        ),
      ),
      EmployeeBarStatistic(
          title: 'Statistics of project on Leobit',
          dataSource: statistic.projectList),
    ];
  }
}
