import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/components/employee_list/employee_item.dart';
import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/selectors/employee_list_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class EmployeeCertificateList extends StatelessWidget {
  const EmployeeCertificateList({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      top: false,
      child: Scaffold(
        key: const Key('employee_list'),
        bottomNavigationBar: const BottomNavigation(currentIndex: 3),
        appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            automaticallyImplyLeading: false,
            title: (Platform.isIOS)
                ? const BackNavigationAppbar(title: 'Certificate Statistic')
                : const SettingsAppBar(title: 'Certificate Statistic')),
        body: StoreConnector<AppState, List<EmployeeData>>(
          converter: (store) => employeeListCertificateSelector(store.state),
          builder: (context, employees) {
            return Container(
              margin: const EdgeInsets.only(top: 12),
              padding: const EdgeInsets.only(top: 8),
              decoration: Platform.isIOS
                  ? BoxDecoration(
                      color: Theme.of(context).colorScheme.background,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0)),
                      border: Border.all(
                          color: Theme.of(context).colorScheme.background))
                  : null,
              child: ListView.separated(
                scrollDirection: Axis.vertical,
                itemCount: employees.length,
                itemBuilder: (context, index) =>
                    Employee(employeeData: employees[index]),
                separatorBuilder: (BuildContext context, int index) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Divider(
                    color: Platform.isIOS ? Colors.transparent : Colors.grey,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
