import 'dart:io' show Platform;

import 'package:employeeviewer/api/model/statistic_model.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class StatisticItem extends StatelessWidget {
  const StatisticItem({super.key, required this.data});

  final BarStatistic data;

  @override
  Widget build(BuildContext context) {
    Store<AppState> store = StoreProvider.of<AppState>(context);

    return InkWell(
      onTap: () {
        store.dispatch(UpdateFilterValueAction('Certificate', data.key));
        Navigator.of(context).pushNamed('/certificate-statistic');
      },
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
            color: Platform.isIOS
                ? Theme.of(context).cardColor
                : Colors.transparent,
            boxShadow: [
              BoxShadow(
                color: Platform.isIOS
                    ? Theme.of(context).cardColor
                    : Colors.transparent,
                spreadRadius: 1,
                blurRadius: 2,
              ),
            ],
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          padding: const EdgeInsets.all(16),
          width: MediaQuery.of(context).size.width * 0.95,
          child: Row(
            children: [
              Expanded(
                flex: 6,
                child: Text(
                  data.key,
                  style: const TextStyle(fontSize: 16),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  data.count.toString(),
                  textAlign: TextAlign.right,
                  style: TextStyle(color: AppColors.leoColor, fontSize: 16),
                ),
              ),
            ],
          )),
    );
  }
}
