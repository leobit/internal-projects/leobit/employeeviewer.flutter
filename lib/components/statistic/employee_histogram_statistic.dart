import 'package:employeeviewer/api/model/statistic_model.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class EmployeeHistogramStatistic extends StatelessWidget {
  const EmployeeHistogramStatistic({
    super.key,
    required this.dataSource,
    required this.title,
  });

  final List<BarStatistic> dataSource;
  final String title;

  @override
  Widget build(BuildContext context) {
    return SfCartesianChart(
        primaryXAxis: CategoryAxis(),
        title: ChartTitle(
          text: title,
          textStyle: const TextStyle(fontSize: 20),
        ),
        series: <CartesianSeries>[
          ColumnSeries<BarStatistic, String>(
              dataSource: dataSource,
              xValueMapper: (BarStatistic data, _) => data.key,
              yValueMapper: (BarStatistic data, _) => data.count,
              color: AppColors.leoColor),
        ]);
  }
}
