import 'dart:io' show Platform;

import 'package:badges/badges.dart' as badges;
import 'package:employeeviewer/redux/actions/tab_navigation_action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/selectors/notification_selector.dart';
import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({super.key, required this.currentIndex});

  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    List<BottomNavigationBarItem> navigationBarItems = [
      const BottomNavigationBarItem(
        icon: Icon(Icons.people),
        label: 'Employee list',
      ),
      if (Platform.isIOS)
        const BottomNavigationBarItem(
          icon: Icon(Icons.favorite_border),
          label: 'Favorite',
        ),
      BottomNavigationBarItem(
        icon: StoreConnector<AppState, int>(
            converter: (store) =>
                notificationCountSelector(store.state.differencesEmployee),
            builder: (context, int count) {
              return badges.Badge(
                showBadge: (count > 0),
                badgeStyle: badges.BadgeStyle(badgeColor: AppColors.leoColor),
                badgeContent: Text(count.toString()),
                child: Icon(
                  (count > 0) ? Icons.notifications : Icons.notifications_none,
                ),
              );
            }),
        label: 'Notification',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.insert_chart),
        label: 'Statistic',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.more_horiz),
        label: 'More',
      ),
    ];

    return StoreBuilder<TabNavigationState>(
        builder: (context, tabNavigationState) => SizedBox(
              child: BottomNavigationBar(
                  iconSize: 30,
                  currentIndex: Platform.isIOS
                      ? currentIndex
                      : (currentIndex > 0)
                          ? currentIndex - 1
                          : 0,
                  onTap: (index) {
                    tabNavigationState.dispatch(BottomNavigate(index));
                    Navigator.of(context)
                        .pushNamed(tabNavigationState.state.navigateDirection);
                  },
                  selectedItemColor: AppColors.leoColor,
                  unselectedItemColor: Theme.of(context).primaryColorLight,
                  items: navigationBarItems),
            ));
  }
}
