import 'package:flutter/material.dart';

class MoreItem extends StatelessWidget {
  const MoreItem({
    super.key,
    required this.title,
    required this.icon,
    required this.onTap,
  });

  final String title;
  final IconData icon;
  final void Function(BuildContext) onTap;

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Icon(icon),
        title: Text(
          title,
          style: const TextStyle(fontSize: 24.0),
        ),
        onTap: () => onTap(context));
  }
}
