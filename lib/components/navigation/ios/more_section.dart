import 'dart:io';

import 'package:employeeviewer/components/navigation/ios/bottom_navigation.dart';
import 'package:employeeviewer/components/navigation/ios/ios_appbar.dart';
import 'package:employeeviewer/components/navigation/ios/more_item.dart';
import 'package:employeeviewer/components/settings_view/settings_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MoreSection extends StatelessWidget {
  const MoreSection({super.key, required this.sectionItems});

  final List<MoreItem> sectionItems;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        bottom: false,
        top: false,
        child: Scaffold(
          appBar: AppBar(
              backgroundColor: Theme.of(context).primaryColor,
              automaticallyImplyLeading: false,
              title: (Platform.isIOS)
                  ? const BackNavigationAppbar(title: 'More')
                  : const SettingsAppBar(title: 'Employee Viewer')),
          bottomNavigationBar: const BottomNavigation(currentIndex: 4),
          body: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 12),
              child: (Platform.isIOS)
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                          sectionItems[0],
                          sectionItems[1],
                          const Divider(
                            color: CupertinoColors.systemGrey,
                          ),
                          sectionItems[2],
                          sectionItems[3]
                        ])
                  : ListView.separated(
                      shrinkWrap: true,
                      itemBuilder: (context, index) => sectionItems[index],
                      separatorBuilder: (context, index) => const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            child: Divider(
                              color: Colors.grey,
                            ),
                          ),
                      itemCount: sectionItems.length),
            ),
          ),
        ));
  }
}
