import 'package:flutter/material.dart';

class BackNavigationAppbar extends StatelessWidget {
  const BackNavigationAppbar({
    super.key,
    this.title,
    this.trailing,
    this.leading,
  });

  final String? title;
  final Widget? trailing;
  final Widget? leading;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        (leading == null)
            ? Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: const Row(
                        children: [
                          Icon(Icons.arrow_back_ios, color: Colors.grey),
                          Text(
                            'Back',
                            style: TextStyle(fontSize: 20, color: Colors.grey),
                          ),
                        ],
                      ),
                    ),
                    Text(
                      title as String,
                      style: TextStyle(
                          fontSize: Theme.of(context)
                              .textTheme
                              .displayLarge
                              ?.fontSize,
                          color: Theme.of(context)
                              .textTheme
                              .displayLarge
                              ?.color
                              ?.withOpacity(1)),
                    ),
                  ],
                ))
            : Expanded(child: leading as Widget),
        (trailing == null)
            ? const Spacer()
            : Expanded(
                child: Align(
                    alignment: Alignment.centerRight,
                    child: trailing as Widget)),
      ],
    );
  }
}
