import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/notification.dart';

class AppState {
  AppState(
      {required this.teamId,
      required this.themeState,
      required this.employeeList,
      required this.filteredEmployeeList,
      required this.dropDownValue,
      required this.filterValue,
      required this.favoriteEmployeeList,
      required this.differencesEmployee,
      required this.savedNotifications,
      required this.archivedNotifications,
      required this.multiselectFilterValue,
      this.employeeData,
      this.personalDetails,
      this.profileData});

  AppState.fromAppState(AppState another) {
    teamId = another.teamId;
    themeState = another.themeState;
    employeeList = another.employeeList;
    employeeData = another.employeeData;
    personalDetails = another.personalDetails;
    profileData = another.profileData;
    filteredEmployeeList = another.filteredEmployeeList;
    favoriteEmployeeList = another.favoriteEmployeeList;
    differencesEmployee = another.differencesEmployee;
    savedNotifications = another.savedNotifications;
    archivedNotifications = another.archivedNotifications;
    dropDownValue = another.dropDownValue;
    filterValue = another.filterValue;
    multiselectFilterValue = another.multiselectFilterValue;
  }

  static AppState init = AppState(
    employeeList: [],
    filteredEmployeeList: [],
    favoriteEmployeeList: [],
    savedNotifications: [],
    themeState: false,
    dropDownValue: {
      'Manager name': <String?>[],
      'Seniority': <String?>[],
      'Profile': <String?>[],
      'Position': <String?>[],
      'Project': <String?>[],
    },
    filterValue: {
      'Employment': 'All',
      'Status': 'All',
      'Name': '',
      'Email': '',
      'Phone': '',
      'Room': '',
      'Certificate': '',
      'Position': <String>[],
      'Profile': <String>[],
      'Seniority': <String>[],
      'Manager name': <String>[],
      'Project': <String>[],
    },
    differencesEmployee: {
      'newcomer': [],
      'leave': [],
      'certificate_update': [],
      'seniority_update': [],
    },
    teamId: 'T2R7H25GC&id',
    multiselectFilterValue: '',
    archivedNotifications: [],
  );

  late String teamId;
  late bool themeState;
  late List<EmployeeData> employeeList;
  late List<EmployeeData> filteredEmployeeList;
  EmployeeData? employeeData;
  EmployeeData? personalDetails;
  PersonalData? profileData;
  late Map<String, List<String?>> dropDownValue;
  late Map<String, dynamic> filterValue;
  late List<String> favoriteEmployeeList;
  late Map<String, List<NotificationUpdate>> differencesEmployee;
  late List<NotificationUpdate> savedNotifications;
  late List<NotificationUpdate> archivedNotifications;
  late String multiselectFilterValue;
}

class TabNavigationState {
  TabNavigationState(
      {required this.tabState,
      required this.statisticTabState,
      required this.navigateDirection});

  TabNavigationState.fromAppState(TabNavigationState another) {
    tabState = another.tabState;
    statisticTabState = another.statisticTabState;
    navigateDirection = another.navigateDirection;
  }

  static TabNavigationState init = TabNavigationState(
      tabState: 'details',
      statisticTabState: 'hiredYear',
      navigateDirection: '/list');

  late String tabState;
  late String statisticTabState;
  late String navigateDirection;
}

class MultiSelectListState {
  MultiSelectListState(
      {required this.selectedNotificationList, required this.isActive});

  MultiSelectListState.fromAppState(MultiSelectListState another) {
    isActive = another.isActive;

    selectedNotificationList = another.selectedNotificationList;
  }

  static MultiSelectListState init =
      MultiSelectListState(selectedNotificationList: [], isActive: false);

  late bool isActive;

  late List<NotificationUpdate> selectedNotificationList;
}
