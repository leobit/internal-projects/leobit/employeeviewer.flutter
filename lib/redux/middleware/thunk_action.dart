import 'package:employeeviewer/api/auth/auth_api.dart';
import 'package:employeeviewer/api/employee_api.dart';
import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/profile_api.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_notification_action.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

const FlutterSecureStorage _storage = FlutterSecureStorage();
final EmployeeApi _employeeApi = EmployeeApi(http.Client());
final ProfileApi _profileApi = ProfileApi(http.Client());
final AuthApi _auth = AuthApi();

ThunkAction<AppState> updateThemeState([bool? isDarkTheme]) {
  return (Store<AppState> store) async {
    if (isDarkTheme != null) {
      await _storage.write(key: 'themeState', value: isDarkTheme.toString());
    }
    store.dispatch(
        SetThemeAction(await _storage.read(key: 'themeState') == 'true'));
  };
}

ThunkAction<AppState> getEmployeeList() {
  return (Store<AppState> store) async {
    List<EmployeeData> employeeList =
        await _employeeApi.getEmployeeData() ?? [];
    String? stringList = await _storage.read(key: 'favorite_list');
    store.state.favoriteEmployeeList =
        ((stringList == null) ? [] : stringList.split(','));
    store.dispatch(DownloadEmployeeList(employeeList));
    store.dispatch(getAllNotifications(NotificationAction.getAllNotification));
  };
}

ThunkAction<AppState> getEmployeeDetails(String employeeId) {
  return (Store<AppState> store) async {
    store.state.employeeData = null;
    EmployeeData employeeDetails =
        (await _employeeApi.getEmployeeDetails(employeeId))!;
    store.dispatch(DownloadEmployeeDetails(employeeDetails));
  };
}

ThunkAction<AppState> filterAndNavigateToEmployeeList(
  BuildContext context,
  EmployeeData employee,
  String filterKey,
  dynamic value,
) {
  return (Store<AppState> store) async {
    store.dispatch(UpdateFilterValueAction(filterKey, value));
    await Navigator.of(context).pushNamed('/list');
    store.dispatch(ClearFilerAction());

    if (store.state.employeeData?.id != employee.id) {
      store.dispatch(SetEmployeeDetails(employee));
    }
  };
}

ThunkAction<AppState> toggleFavoriteEmployee(String employeeId) {
  return (Store<AppState> store) async {
    store.dispatch(UpdateFavoriteState(employeeId));

    await _storage.write(
        key: 'favorite_list',
        value: store.state.favoriteEmployeeList.join(','));
  };
}

ThunkAction<AppState> loginUser(BuildContext context) {
  return (Store<AppState> store) async {
    PlatformException? error = await _auth.authUser(context);
    if (error != null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content:
            Text('Something went wrong \nError: ${error.message as String}'),
      ));
    }
  };
}

ThunkAction<AppState> loadProfileData() {
  return (Store<AppState> store) async {
    store.state.profileData ??= await _profileApi.getData();
    EmployeeData profileDetails =
        (await _employeeApi.getEmployeeDetails(store.state.profileData!.sub!))!;
    store.dispatch(DownloadEmployeeDetails(profileDetails));
  };
}

ThunkAction<AppState> logoutUser(BuildContext context) {
  return (Store<AppState> store) async {
    try {
      FirebaseCrashlytics.instance
          .log('Logout User|INFO|\n${store.state.profileData.toString()}');
      await _auth.logoutUser();
    } catch (error, stackTrace) {
      await FirebaseCrashlytics.instance
          .recordError(error, stackTrace, reason: 'Failed logout user');
    } finally {
      store.state.profileData = null;
      await _auth.clearStorage();
      Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
    }
  };
}
