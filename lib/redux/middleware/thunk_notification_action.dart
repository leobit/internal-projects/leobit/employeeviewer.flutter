import 'package:async/async.dart';
import 'package:collection/collection.dart';
import 'package:employeeviewer/api/employee_api.dart';
import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/notification.dart';
import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/actions/update_notification.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:http/http.dart' as http;
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:timezone/timezone.dart' as tz;

final EmployeeApi _employeeApi = EmployeeApi(http.Client());

final UpdateNotification _notification = UpdateNotification();
final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

late RestartableTimer _restartableTimer;

ThunkAction<AppState> updateStoredNotification(List<NotificationUpdate> data) {
  return (Store<AppState> store) async {
    var box = await Hive.openBox<List>('employee_data');
    store.state.savedNotifications.addAll(data.map((e) => NotificationUpdate(
        isChecked: true,
        id: e.id,
        fullName: e.fullName,
        photoSmall: e.photoSmall,
        notificationType: e.notificationType,
        oldData: e.oldData,
        newData: e.newData)));

    store.dispatch(updateStoredEmployeeList(data));
    await box.put('notification_list', store.state.savedNotifications);
  };
}

ThunkAction<AppState> updateStoredEmployeeList(List<NotificationUpdate> data) {
  return (Store<AppState> store) async {
    var box = await Hive.openBox<List>('employee_data');
    List<EmployeeData> oldListEmployee = [...?box.get('employee_list')],
        currentListEmployee = [...store.state.employeeList];
    List<String> employeeListId = oldListEmployee.map((e) => e.id).toList();
    for (var notification in data) {
      if (employeeListId.contains(notification.id)) {
        oldListEmployee.removeWhere((element) => element.id == notification.id);
      }
      oldListEmployee.add(currentListEmployee
          .firstWhere((element) => element.id == notification.id));
      for (var element in oldListEmployee) {
        if (element.id == notification.id) {
          store.state.differencesEmployee[notification.notificationType]
              ?.removeWhere((element) => element.id == notification.id);
        }
      }
    }

    store.dispatch(UpdateStoredNotification());
    await box.put('employee_list', oldListEmployee);
  };
}

ThunkAction<AppState> archiveNotifications(List<NotificationUpdate> data) {
  return (Store<AppState> store) async {
    var box = await Hive.openBox<List>('employee_data');
    store.state.archivedNotifications.addAll(data);
    await box.put('archived_notifications', store.state.archivedNotifications);
    for (var archive in data) {
      store.dispatch(updateStoredEmployeeList([archive]));
      if (archive.isChecked) {
        store.state.savedNotifications.removeWhere((element) =>
            '${element.id}${element.notificationType}' ==
            '${archive.id}${archive.notificationType}');
      }
    }
  };
}

ThunkAction<AppState> getAllNotifications(NotificationAction state) {
  return (Store<AppState> store) async {
    var box = await Hive.openBox<List>('employee_data');
    List<EmployeeData> oldListEmployee = [], currentListEmployee = [];
    if (currentListEmployee.isEmpty) {
      currentListEmployee = [
        ...store.state.employeeList.map((element) => element)
      ];
    }
    if (store.state.archivedNotifications.isEmpty) {
      store.state.archivedNotifications = [
        ...?box.get('archived_notifications')
      ];
    }
    if (box.get('employee_list') == null) {
      await box.put('employee_list', currentListEmployee);
      oldListEmployee = [...currentListEmployee];
    }
    if (state == NotificationAction.clearStoredNotification) {
      store.state.savedNotifications = [];
      store.state.archivedNotifications = [];
      await box.put('notification_list', store.state.savedNotifications);
      await box.put(
          'archived_notifications', store.state.archivedNotifications);
    }

    if (oldListEmployee.isEmpty) {
      oldListEmployee = [...?box.get('employee_list')];
    }

    store.state.differencesEmployee = _notification.updateEmployeeNotification(
        store.state.differencesEmployee, oldListEmployee, currentListEmployee);
    store.state.differencesEmployee =
        _notification.updateEmployeeAnotherNotification(
            store.state.differencesEmployee,
            oldListEmployee,
            currentListEmployee,
            'certificate_update');
    store.state.differencesEmployee =
        _notification.updateEmployeeAnotherNotification(
            store.state.differencesEmployee,
            oldListEmployee,
            currentListEmployee,
            'seniority_update');

    if (box.get('notification_list') == null) {
      await box.put('notification_list', store.state.savedNotifications);
    }

    if (store.state.savedNotifications.isEmpty) {
      store.state.savedNotifications = [...?box.get('notification_list')];
    }

    startScheduledNotification(
        store.state, currentListEmployee, oldListEmployee);
  };
}

void startScheduledNotification(
    AppState state,
    List<EmployeeData> currentListEmployee,
    List<EmployeeData> oldListEmployee) async {
  var androidPlatformChannelSpecifics = const AndroidNotificationDetails(
    'employee_notification',
    'Employee Viewer',
    icon: 'ic_launcher',
    importance: Importance.high,
    playSound: true,
    priority: Priority.high,
  );
  var iOSPlatformChannelSpecifics = const DarwinNotificationDetails(
      presentAlert: true, presentBadge: true, presentSound: true);
  var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
  _restartableTimer = RestartableTimer(
    const Duration(hours: 3),
    () async {
      final employeeData = await _employeeApi.getEmployeeData() ?? [];
      currentListEmployee = [...employeeData];
      state.differencesEmployee = _notification.updateEmployeeNotification(
          state.differencesEmployee, oldListEmployee, currentListEmployee);
      if (state.differencesEmployee.values
              .map((value) => value.length)
              .toList()
              .sum !=
          0) {
        int updateCount = state.differencesEmployee.values
            .map((value) => value.length)
            .toList()
            .sum;
        await _flutterLocalNotificationsPlugin
            .zonedSchedule(
              0,
              'Employee viewer',
              'Check the app, you have ( $updateCount ) updates',
              tz.TZDateTime.now(tz.local).add(const Duration(seconds: 10)),
              platformChannelSpecifics,
              uiLocalNotificationDateInterpretation:
                  UILocalNotificationDateInterpretation.absoluteTime,
              androidAllowWhileIdle: true,
            )
            .then((value) => _restartableTimer.reset());
      } else {
        _restartableTimer.reset();
      }
    },
  );
}
