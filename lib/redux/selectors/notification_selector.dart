import 'package:collection/collection.dart';
import 'package:employeeviewer/api/model/notification.dart';

int notificationCountSelector(
    Map<String, List<NotificationUpdate>> notifications) {
  return notifications.values.map((value) => value.length).toList().sum;
}
