import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/statistic_model.dart';
import 'package:employeeviewer/redux/actions/update_statistic.dart';
import 'package:employeeviewer/redux/app_state.dart';

List<EmployeeData> employeeListCertificateSelector(AppState state) {
  return state.employeeList
      .where((element) =>
          element.certificates != null &&
          element.certificates!
              .split('\n')
              .contains(state.filterValue['Certificate']))
      .toList();
}

StatisticModel employeeStatisticSelector(AppState state) {
  return UpdateStatistic()
      .getStatistic(state.employeeList, state.dropDownValue);
}

bool favoriteEmployeeSelector(AppState state, String employeeId) {
  return state.favoriteEmployeeList.contains(employeeId);
}

List<String?> filteredMultiselectListSelector(
    AppState state, List<String?> filterData) {
  return filterData
      .where((element) => element!
          .toLowerCase()
          .contains(state.multiselectFilterValue.toLowerCase()))
      .toList();
}
