import 'package:employeeviewer/redux/actions/multi_select_list_action.dart';
import 'package:employeeviewer/redux/app_state.dart';

MultiSelectListState multiselectReducer(
    MultiSelectListState prevState, dynamic action) {
  MultiSelectListState newState = MultiSelectListState.fromAppState(prevState);

  if (action is InitAction) {
    newState.isActive = action.isActive;
    newState.selectedNotificationList = [];
  }
  if (action is AddSelectedItemAction) {
    if (newState.selectedNotificationList
        .map((notification) =>
            "${notification.id}${notification.notificationType}")
        .toList()
        .contains("${action.item.id}${action.item.notificationType}")) {
      newState.selectedNotificationList.removeWhere((element) =>
          "${element.id}${element.notificationType}" ==
          "${action.item.id}${action.item.notificationType}");
    } else {
      newState.selectedNotificationList.add(action.item);
    }
  }

  return newState;
}
