import 'dart:io' show Platform;

import 'package:employeeviewer/redux/actions/tab_navigation_action.dart';
import 'package:employeeviewer/redux/app_state.dart';

TabNavigationState tabNavigationReducer(
    TabNavigationState prevState, dynamic action) {
  TabNavigationState newState = TabNavigationState.fromAppState(prevState);
  if (action is UpdateTabState) {
    newState.tabState = action.tabValue;
  }

  if (action is UpdateStatisticTabState) {
    newState.statisticTabState = action.tabValue;
  }
  if (action is BottomNavigate) {
    var switchIndex = Platform.isIOS
        ? action.index
        : (action.index > 0)
            ? action.index + 1
            : 0;
    switch (switchIndex) {
      case 0:
        newState.navigateDirection = '/list';
        break;

      case 1:
        newState.navigateDirection = '/favorite';
        break;

      case 2:
        newState.navigateDirection = '/notification';
        break;

      case 3:
        newState.navigateDirection = '/statistic';
        break;

      case 4:
        newState.navigateDirection = '/more';
    }
  }
  return newState;
}
