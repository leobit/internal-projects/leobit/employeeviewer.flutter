import 'package:employeeviewer/redux/actions/action.dart';
import 'package:employeeviewer/redux/actions/filter_action.dart';
import 'package:employeeviewer/redux/app_state.dart';

AppState reducer(AppState prevState, dynamic action) {
  AppState newState = AppState.fromAppState(prevState);

  if (action is SetThemeAction) {
    newState.themeState = action.isDarkTheme;
  }

  if (action is DownloadEmployeeList) {
    newState.employeeList = action.employeeList;
    newState.dropDownValue = FilterAction.updateDropdownFilter(
        newState.employeeList, newState.dropDownValue);
  }

  if (action is DownloadEmployeeDetails) {
    newState.personalDetails ??= action.employeeData;
    newState.employeeData = action.employeeData;
  }

  if (action is SetEmployeeDetails) {
    newState.employeeData = action.employeeData;
  }

  if (action is ClearFilerAction) {
    newState.filterValue = FilterAction.clearFilterValue(newState.filterValue);
  }

  if (action is UpdateFavoriteState) {
    newState.favoriteEmployeeList.contains(action.employeeId)
        ? newState.favoriteEmployeeList.remove(action.employeeId)
        : newState.favoriteEmployeeList.add(action.employeeId);
  }

  if (action is UpdateFilterValueAction) {
    if (action.filterKey != null) {
      newState.filterValue
          .update(action.filterKey as String, (keyValue) => action.value);
    }
  }

  if (action is UpdateModalityFilterValueAction) {
    if (action.filterKey != null) {
      (newState.filterValue[action.filterKey] as List).contains(action.value)
          ? (newState.filterValue[action.filterKey] as List)
              .remove(action.value)
          : (newState.filterValue[action.filterKey] as List).add(action.value);
    }
  }

  if (action is PrepareMultiselectFilter) {
    newState.multiselectFilterValue = '';
  }

  if (action is UpdateMultiselectFilterList) {
    newState.multiselectFilterValue = action.filterValue;
  }

  newState.filteredEmployeeList = [...newState.employeeList];
  newState.filterValue.forEach((key, keyValue) {
    newState.filteredEmployeeList = FilterAction.filterEmployeeList(
        key, keyValue, newState.filteredEmployeeList, newState.filterValue);
  });
  return newState;
}
