import 'package:collection/collection.dart';
import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/notification.dart';

class UpdateNotification {
  Map<String, List<NotificationUpdate>> updateEmployeeNotification(
      Map<String, List<NotificationUpdate>> notification,
      List<EmployeeData> oldListEmployee,
      List<EmployeeData> currentListEmployee) {
    notification.update(
        'leave',
        (value) => [
              ...oldListEmployee
                  .where((element) => (oldListEmployee
                          .where((element) => element.active)
                          .map((e) => e.id)
                          .toSet()
                          .difference(currentListEmployee
                              .where((element) => element.active)
                              .map((e) => e.id)
                              .toSet())
                          .toList())
                      .contains(element.id))
                  .map((employee) => NotificationUpdate(
                        id: employee.id,
                        notificationType: 'leave',
                        fullName: employee.fullName,
                        photoSmall: employee.photoSmall,
                        isChecked: false,
                      ))
            ]);
    notification.update(
        'newcomer',
        (value) => [
              ...currentListEmployee
                  .where((element) => (currentListEmployee
                          .where((element) => element.active)
                          .map((e) => e.id)
                          .toSet()
                          .difference(oldListEmployee
                              .where((element) => element.active)
                              .map((e) => e.id)
                              .toSet())
                          .toList())
                      .contains(element.id))
                  .map((employee) => NotificationUpdate(
                        id: employee.id,
                        fullName: employee.fullName,
                        photoSmall: employee.photoSmall,
                        notificationType: 'newcomer',
                        isChecked: false,
                      ))
            ]);
    return notification;
  }

  Map<String, List<NotificationUpdate>> updateEmployeeAnotherNotification(
      Map<String, List<NotificationUpdate>> notification,
      List<EmployeeData> oldListEmployee,
      List<EmployeeData> currentListEmployee,
      String updateTitle) {
    if (updateTitle == 'certificate_update') {
      notification.update(
          updateTitle,
          (value) => [
                ...oldListEmployee
                    .where((element) => (oldListEmployee
                            .map((e) => e.id + e.certificates.toString())
                            .toSet()
                            .difference(currentListEmployee
                                .map((e) => e.id + e.certificates.toString())
                                .toSet())
                            .toList())
                        .contains(element.id + element.certificates.toString()))
                    .map((employee) => NotificationUpdate(
                        id: employee.id,
                        fullName: employee.fullName,
                        photoSmall: employee.photoSmall,
                        isChecked: false,
                        notificationType: 'certificate_update',
                        oldData: oldListEmployee.firstWhereOrNull(
                            (element) => element.id == employee.id),
                        newData: currentListEmployee.firstWhereOrNull(
                            (element) => element.id == employee.id)))
              ]);
    } else {
      notification.update(
          updateTitle,
          (value) => [
                ...oldListEmployee
                    .where((element) => (oldListEmployee
                            .map((e) => e.id + e.seniority.toString())
                            .toSet()
                            .difference(currentListEmployee
                                .map((e) => e.id + e.seniority.toString())
                                .toSet())
                            .toList())
                        .contains(element.id + element.seniority.toString()))
                    .map((employee) => NotificationUpdate(
                        id: employee.id,
                        fullName: employee.fullName,
                        photoSmall: employee.photoSmall,
                        notificationType: 'seniority_update',
                        isChecked: false,
                        oldData: oldListEmployee.firstWhereOrNull(
                            (element) => element.id == employee.id),
                        newData: currentListEmployee.firstWhereOrNull(
                            (element) => element.id == employee.id)))
              ]);
    }
    return notification;
  }
}
