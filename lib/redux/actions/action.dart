import 'package:employeeviewer/api/model/api_models.dart';

class SetThemeAction {
  const SetThemeAction(this.isDarkTheme);

  final bool isDarkTheme;
}

class DownloadEmployeeList {
  DownloadEmployeeList(this.employeeList);

  List<EmployeeData> employeeList;
}

class DownloadEmployeeDetails {
  DownloadEmployeeDetails(this.employeeData);

  EmployeeData employeeData;
}

class SetEmployeeDetails {
  SetEmployeeDetails(this.employeeData);

  EmployeeData employeeData;
}

class UpdateFavoriteState {
  UpdateFavoriteState(this.employeeId);

  final String employeeId;
}

class UpdateFilterValueAction {
  UpdateFilterValueAction(this.filterKey, this.value);

  final String? filterKey;
  final dynamic value;
}

class UpdateModalityFilterValueAction {
  UpdateModalityFilterValueAction(this.filterKey, this.value);

  final String? filterKey;
  final dynamic value;
}

class ClearFilerAction {
  ClearFilerAction();
}

class UpdateMultiselectFilterList {
  UpdateMultiselectFilterList(this.dataList, this.filterValue);

  final List<String?> dataList;
  final String filterValue;
}

class PrepareMultiselectFilter {
  PrepareMultiselectFilter();
}

class UpdateStoredNotification {
  UpdateStoredNotification();
}

enum NotificationAction { getAllNotification, clearStoredNotification }
