import 'package:employeeviewer/api/model/api_models.dart';

class FilterAction {
  static Map<String, dynamic> clearFilterValue(Map<String, dynamic> filter) {
    filter.forEach((key, value) {
      switch (key) {
        case 'Name':
          filter[key] = '';
          break;
        case 'Email':
          filter[key] = '';
          break;
        case 'Phone':
          filter[key] = '';
          break;
        case 'Room':
          filter[key] = '';
          break;
        case 'Employment':
          filter[key] = 'All';
          break;
        case 'Status':
          filter[key] = 'All';
          break;
        case 'Position':
          filter[key] = [];
          break;
        case 'Profile':
          filter[key] = [];
          break;
        case 'Seniority':
          filter[key] = [];
          break;
        case 'Manager name':
          filter[key] = [];
          break;
        case 'Project':
          filter[key] = [];
          break;
      }
    });
    return filter;
  }

  static List<EmployeeData> filterEmployeeList(
      String key, value, List<EmployeeData> data, Map filterValue) {
    switch (key) {
      case 'Name':
        data = data
            .where((element) =>
                element.fullName.toLowerCase().contains(value.toLowerCase()))
            .toList();

        break;
      case 'Email':
        data = data
            .where((element) =>
                element.email.toLowerCase().contains(value.toLowerCase()))
            .toList();

        break;
      case 'Phone':
        data = data
            .where((element) => (element.phone ?? '')
                .toLowerCase()
                .contains(value.toLowerCase()))
            .toList();

        break;
      case 'Room':
        data = data
            .where((element) =>
                element.roomNumber.toLowerCase().contains(value.toLowerCase()))
            .toList();

        break;
      case 'Employment':
        data = (value == 'All')
            ? data
            : data.where((element) => (element.employment == value)).toList();

        break;
      case 'Status':
        data = (value == 'All')
            ? data
            : data
                .where((element) =>
                    ((value == 'Active') ? element.active : !element.active))
                .toList();

        break;

      case 'Position':
        List filterList = filterValue['Position'];
        data = (filterList.isEmpty)
            ? data
            : data
                .where((element) => filterList.contains(element.position))
                .toList();

        break;

      case 'Profile':
        List filterList = filterValue['Profile'];
        data = (filterList.isEmpty)
            ? data
            : data
                .where((element) => filterList.contains(element.profile))
                .toList();

        break;
      case 'Seniority':
        List filterList = filterValue['Seniority'];
        data = (filterList.isEmpty)
            ? data
            : data
                .where((element) => filterList.contains(element.seniority))
                .toList();

        break;
      case 'Manager name':
        List filterList = filterValue['Manager name'];
        data = (filterList.isEmpty)
            ? data
            : data
                .where((element) => filterList.contains(element.manager))
                .toList();

        break;
      case 'Project':
        List filterList = filterValue['Project'];
        data = filterList.isEmpty
            ? data
            : data
                .where(
                  (element) => element.projects == null
                      ? false
                      : filterList.any((filterItem) =>
                          element.projects!.contains(filterItem)),
                )
                .toList();

        break;
    }
    return data;
  }

  static Map<String, List<String?>> updateDropdownFilter(
      List<EmployeeData> dataList, Map<String, List<String?>> dropDownValue) {
    dropDownValue.update(
        'Manager name',
        (value) => ([
              ...{...dataList.map((element) => element.manager)}
            ].where((element) => element != null)).toList());

    dropDownValue.update(
        'Position',
        (value) => ([
              ...{...dataList.map((element) => element.position)}
            ].where((element) => element != null)).toList());

    dropDownValue.update(
        'Profile',
        (value) => ([
              ...{...dataList.map((element) => element.profile)}
            ].where((element) => element != null)).toList());

    dropDownValue.update(
        'Seniority',
        (value) => ([
              ...{...dataList.map((element) => element.seniority)}
            ].where((element) => element != null)).toList());

    dropDownValue.update('Project', (value) {
      final projects = <String>[];
      for (final data in dataList) {
        if (data.projects != null) {
          for (final project in data.projects!) {
            if (project != null && !projects.contains(project)) {
              projects.add(project);
            }
          }
        }
      }
      projects.sort();

      return projects;
    });

    return dropDownValue;
  }
}
