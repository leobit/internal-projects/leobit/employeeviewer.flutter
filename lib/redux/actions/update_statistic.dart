import 'package:employeeviewer/api/model/api_models.dart';
import 'package:employeeviewer/api/model/statistic_model.dart';

class UpdateStatistic {
  StatisticModel getStatistic(List<EmployeeData> employeeList,
      Map<String, List<String?>> dropDownValue) {
    return StatisticModel(
      yearHiredList: generateYearHiredList(employeeList),
      totalExperienceList: generateTotalExperienceList(employeeList),
      projectManagerList:
          generateManagerList(dropDownValue['Manager name'], employeeList),
      seniorityList:
          generateSeniorityList(dropDownValue['Seniority'], employeeList),
      profileList: generateProfileList(dropDownValue['Profile'], employeeList),
      certificateList: generateCertificateList(employeeList),
      projectList: generateProjectList(employeeList),
    );
  }

  List<BarStatistic> generateYearHiredList(List<EmployeeData> employeeList) {
    List<String> yearList = employeeList
        .where((element) => element.active && element.yearHired != null)
        .map((e) => e.yearHired!)
        .toList();
    return yearList
        .toSet()
        .map((year) => BarStatistic(
            key: year,
            count: yearList.where((element) => element == year).length))
        .toList();
  }

  List<BarStatistic> generateTotalExperienceList(
      List<EmployeeData> employeeList) {
    List<String> experienceList = employeeList
        .where((element) => element.active && element.yearHired != null)
        .map((e) => (DateTime.now().year - int.parse(e.yearHired!)).toString())
        .toList();
    return experienceList
        .toSet()
        .map((experience) => BarStatistic(
            key: (experience == '0') ? '>0' : experience,
            count: experienceList
                .where((element) => element == experience)
                .length))
        .toList();
  }

  List<BarStatistic> generateProfileList(
      List<String?>? profileList, List<EmployeeData> employeeList) {
    final profileBarStatistics = profileList!
        .map((profile) => BarStatistic(
            key: profile!,
            count: employeeList
                .where((element) => element.profile == profile)
                .length))
        .toList();

    profileBarStatistics.sort((a, b) => a.count.compareTo(b.count));

    return profileBarStatistics;
  }

  List<BarStatistic> generateManagerList(
      List<String?>? managerList, List<EmployeeData> employeeList) {
    final managerBarStatistics = employeeList
        .where((element) =>
            element.active && managerList!.contains(element.fullName))
        .map((e) => e.fullName)
        .toList()
        .map((manager) => BarStatistic(
            key: manager,
            count: employeeList
                .where(
                    (element) => element.active && element.manager == manager)
                .length))
        .toList();

    managerBarStatistics.sort((a, b) => a.count.compareTo(b.count));

    return managerBarStatistics;
  }

  List<BarStatistic> generateSeniorityList(
      List<String?>? seniorityList, List<EmployeeData> employeeList) {
    final seniorityBarStatistics = seniorityList!
        .map((seniority) => BarStatistic(
            key: seniority!,
            count: employeeList
                .where((element) => element.seniority == seniority)
                .length))
        .toList();

    seniorityBarStatistics.sort((a, b) => a.count.compareTo(b.count));

    return seniorityBarStatistics;
  }

  List<BarStatistic> generateCertificateList(List<EmployeeData> employeeList) {
    Set<String> uniqueCertificate = {};

    employeeList.where((element) => element.certificates != null).forEach(
        (element) =>
            uniqueCertificate.addAll(element.certificates!.split('\n')));

    final certificateBarStatistics = uniqueCertificate
        .map((certificate) => BarStatistic(
            key: certificate,
            count: employeeList
                .where((element) =>
                    element.certificates != null &&
                    element.certificates!.split('\n').contains(certificate))
                .length))
        .toList();

    certificateBarStatistics.sort((a, b) => b.count.compareTo(a.count));

    return certificateBarStatistics;
  }

  List<BarStatistic> generateProjectList(List<EmployeeData> employeeList) {
    final projectCount = <String, int>{};

    for (final employee in employeeList) {
      if (employee.active && employee.projects != null) {
        final projects = Set<String>.from(employee.projects!);
        for (final project in projects) {
          if (!project.startsWith('Leobit')) {
            if (projectCount.containsKey(project)) {
              projectCount[project] = projectCount[project]! + 1;
            } else {
              projectCount[project] = 1;
            }
          }
        }
      }
    }

    final projectBarStatistics = [
      for (final entry in projectCount.entries)
        BarStatistic(key: entry.key, count: entry.value),
    ];

    projectBarStatistics.sort((a, b) => a.count.compareTo(b.count));

    return projectBarStatistics;
  }
}
