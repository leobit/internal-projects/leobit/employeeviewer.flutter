import 'package:employeeviewer/api/model/notification.dart';

class AddSelectedItemAction {
  const AddSelectedItemAction(this.item);

  final NotificationUpdate item;
}

class InitAction {
  const InitAction(this.isActive);

  final bool isActive;
}
