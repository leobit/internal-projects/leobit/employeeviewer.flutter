class UpdateTabState {
  UpdateTabState(this.tabValue);

  final String tabValue;
}

class UpdateStatisticTabState {
  UpdateStatisticTabState(this.tabValue);

  final String tabValue;
}

class BottomNavigate {
  BottomNavigate(this.index);

  final int index;
}
