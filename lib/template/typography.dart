import 'package:employeeviewer/template/colors.dart';
import 'package:flutter/cupertino.dart';

class AppTypography {
  static TextStyle headline1 = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 20,
      color: AppColors.black.withOpacity(0.54));

  static TextStyle headline2 = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 19,
      color: AppColors.black.withOpacity(0.38));

  static TextStyle headline3 = TextStyle(
      fontWeight: FontWeight.w800, fontSize: 22, color: AppColors.black);

  static TextStyle headline4 = const TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );
  static TextStyle headline5 =
      const TextStyle(color: CupertinoColors.systemGrey);

  static TextStyle headline6 = const TextStyle(
      fontSize: 14,
      color: CupertinoColors.systemGrey,
      fontWeight: FontWeight.w500);

  static TextStyle darkThemeHeadline1 = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 20,
      color: AppColors.white.withOpacity(0.9));

  static TextStyle darkThemeHeadline2 = TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 19,
      color: AppColors.white.withOpacity(0.54));

  static TextStyle darkThemeHeadline3 = TextStyle(
      fontWeight: FontWeight.w800,
      fontSize: 22,
      color: AppColors.white.withOpacity(0.8));

  static TextStyle darkThemeHeadline4 = TextStyle(
    color: AppColors.white.withOpacity(0.8),
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );
  static TextStyle darkThemeHeadline5 = TextStyle(
    color: CupertinoColors.white.withOpacity(0.8),
  );
  static TextStyle darkThemeHeadline6 = const TextStyle(
      fontSize: 14, color: CupertinoColors.white, fontWeight: FontWeight.w500);
}
