import 'package:employeeviewer/components/navigation/ios/more_item.dart';
import 'package:employeeviewer/redux/app_state.dart';
import 'package:employeeviewer/redux/middleware/thunk_action.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

final List<MoreItem> moreItemList = [
  MoreItem(
      title: 'Profile',
      icon: Icons.person,
      onTap: (context) {
        StoreProvider.of<AppState>(context).dispatch(getEmployeeDetails(
            StoreProvider.of<AppState>(context).state.personalDetails!.id));
        Navigator.of(context).pushNamed('/details',
            arguments:
                StoreProvider.of<AppState>(context).state.personalDetails!.id);
      }),
  MoreItem(
      title: 'Corporate Rules',
      icon: Icons.privacy_tip,
      onTap: (context) => Navigator.of(context).pushNamed('/corporate-rules')),
  MoreItem(
      title: 'Settings',
      icon: Icons.settings,
      onTap: (context) => Navigator.of(context).pushNamed('/settings')),
  MoreItem(
    title: 'Logout',
    icon: Icons.logout,
    onTap: (context) =>
        StoreProvider.of<AppState>(context).dispatch(logoutUser(context)),
  ),
];
