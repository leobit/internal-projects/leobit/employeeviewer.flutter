import 'package:employeeviewer/api/model/api_models.dart';

final List<CorporateRulesTitle> corporateRulesTitleList = [
  CorporateRulesTitle(urlList: [
    CorporateRulesData(
        title: 'Useful password',
        url:
            'https://drive.google.com/open?id=17fxjOPMoErzDNnxYPW_SH_M3D7CuNX3V'),
    CorporateRulesData(
        title: 'Instructions for the printer',
        url:
            'https://drive.google.com/open?id=1bIWnaDyMb8oK9kJ86wWeviA6ESM2kJKh'),
    CorporateRulesData(
        title: 'Test devices request form',
        url:
            'https://docs.google.com/forms/d/e/1FAIpQLSfep9eHWLY7hPqUx2Be5taPcWLsDbGKPzvoQbcuDzJKGrB_Hw/viewform'),
    CorporateRulesData(
        title: 'List of test devices',
        url:
            'https://docs.google.com/spreadsheets/d/1xXA5SbSJlvo49mWRBAY-3awNUf4JintT6mw50HJEaeM/edit?usp=sharing'),
  ], topic: 'Useful company links'),
  CorporateRulesTitle(urlList: [
    CorporateRulesData(
        title: 'Corporate rules document',
        url:
            'https://docs.google.com/document/d/1LmrszYTuJrHTmwO9nX0lwmvIKEquLUxqXGSnaJ7DflU'),
    CorporateRulesData(
        title: 'Time Reporting Guideline',
        url:
            'https://drive.google.com/open?id=1spLDty-oK6h1ASenZ9dpPEBpg3flEZo8'),
    CorporateRulesData(
        title: 'Leobit Official Holidays 2022',
        url:
            'https://docs.google.com/document/d/1NsYvft1-G9QVG3s6hncPqFdIAagJFM5oxf65rWFa8zo/edit'),
    CorporateRulesData(
        title: 'Voluntary health insurance',
        url:
            'https://docs.google.com/spreadsheets/d/1mMgFKKLWux95_uGmAAbPg9bRbcLHQqo_/edit?usp=sharing&ouid=107857688099630047992&rtpof=true&sd=true'),
  ], topic: 'Working conditions & rules'),
  CorporateRulesTitle(urlList: [
    CorporateRulesData(
        title: 'Promotion criteria',
        url:
            'https://docs.google.com/spreadsheets/d/1Ts_vWSh1vEcuRD2RNget8lFpK0nFvm7X5NmAkmVCr-k/edit#gid=0'),
    CorporateRulesData(
        title: 'Knowledge Base',
        url:
            'https://docs.google.com/spreadsheets/d/13twCsmIMv1OQ-l4dbRmUDRsUASQFLR0Ufi553ixC_u0/edit#gid=1786245669'),
  ], topic: 'Training and development'),
];
