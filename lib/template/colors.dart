import 'package:flutter/material.dart';

class AppColors {
  static Color white = Colors.white;
  static Color black = Colors.black;
  static Color leoColor = const Color.fromRGBO(96, 178, 58, 1);
  static Color lightGreen = Colors.lightGreenAccent;
}
