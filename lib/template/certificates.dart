final Map<String, CertificatePath> certificateImages = {
  'ISTQB: Agile - Agile Tester': CertificatePath(
    path:
        'https://employee.leobit.co/assets/certificates/ISTQB_AgileTester.png',
    local: false,
  ),
  'ISTQB: Core - Foundation Level': CertificatePath(
    path:
        'https://employee.leobit.co/assets/certificates/ISTQB_FoundationLevel.png',
    local: false,
  ),
  'ISTQB Advanced Level - Test Manager': CertificatePath(
    path:
        'https://employee.leobit.co/assets/certificates/ISTQB_Advanced_Level-Test_Manager.png',
    local: false,
  ),
  'MCSD  Exam 70-483': CertificatePath(
    path: 'https://employee.leobit.co/assets/certificates/MCSD_Exam_70_483.png',
    local: false,
  ),
  'MCSD  Exam 70-486': CertificatePath(
    path: 'https://employee.leobit.co/assets/certificates/MCSD_Exam_70_486.png',
    local: false,
  ),
  'MCSD  Exam 70-487': CertificatePath(
    path: 'https://employee.leobit.co/assets/certificates/MCSD_Exam_70_487.png',
    local: false,
  ),
  'Microsoft Certified: Azure Developer Associate': CertificatePath(
    path: 'https://employee.leobit.co/assets/certificates/MCSD_Exam_AZ-204.png',
    local: false,
  ),
  'Microsoft Certified: Azure Solutions Architect Expert': CertificatePath(
    path:
        'https://employee.leobit.co/assets/certificates/MCSD_Exam_AZ-303+304.png',
    local: false,
  ),
  'Scrum Master': CertificatePath(
    path: 'https://employee.leobit.co/assets/certificates/Scrum_Master.png',
    local: false,
  ),
  'Exam AZ-900 Microsoft Azure Fundamentals': CertificatePath(
    path: 'assets/images/certificates/MCSD_Exam_AZ-900.png',
    local: true,
  ),
  'ISTQB  Foundation Level Extension, Agile Tester': CertificatePath(
    path: 'assets/images/certificates/ISTQB_Foundation_Level-Agile_Tester.png',
    local: true,
  ),
  'ISTQB Advanced Level - Test Analyst': CertificatePath(
    path: 'assets/images/certificates/ISTQB_Advanced_Level-Test_Analyst.png',
    local: true,
  ),
};

class CertificatePath {
  CertificatePath({
    required this.path,
    required this.local,
  });

  final String path;
  final bool local;
}
